/*
 * Copyright (c) 2010-2012, 2014-2019 ARM Limited
 * Copyright (c) 2013 Advanced Micro Devices, Inc.
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2004-2006 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __CPU_O3_RENAME_IMPL_HH__
#define __CPU_O3_RENAME_IMPL_HH__

#include <list>

#include "arch/registers.hh"
#include "config/the_isa.hh"
#include "cpu/o3/rename.hh"
#include "cpu/reg_class.hh"
#include "cpu/utils.hh"
#include "debug/Activity.hh"
#include "debug/O3PipeView.hh"
#include "debug/Rename.hh"
#include "params/DerivO3CPU.hh"

using namespace std;

template <class Impl>
DefaultRename<Impl>::DefaultRename(O3CPU *_cpu, const DerivO3CPUParams &params)
    : cpu(_cpu),
      enableRenameIdioms(params.enableRenameIdioms),
      enableExtendedRenameIdioms(params.enableExtendedRenameIdioms),
      enableEarlyBranchResolution(params.enableEarlyBranchResolution),
      enableBranchZeroPred(params.enableBranchZeroPred),
      enableSubsConstPred(params.enableSubsConstPred),
      enableMoveElimination(params.enableMoveElimination),
      enableDynStrReduction(params.enableDynStrReduction),
      enableEarlyALU(params.enableEarlyALU),
      enableSmallConsts(params.enableSmallConsts),
      iewToRenameDelay(params.iewToRenameDelay),
      decodeToRenameDelay(params.decodeToRenameDelay),
      commitToRenameDelay(params.commitToRenameDelay),
      renameWidth(params.renameWidth),
      commitWidth(params.commitWidth),
      numThreads(params.numThreads),
      stats(_cpu)
#ifdef ENABLE_SILENT_STORE_DETECTION
      , renamed_mem_reg(128, {-1, -1})
#endif
{
    if (renameWidth > Impl::MaxWidth)
        fatal("renameWidth (%d) is larger than compiled limit (%d),\n"
             "\tincrease MaxWidth in src/cpu/o3/impl.hh\n",
             renameWidth, static_cast<int>(Impl::MaxWidth));

    // @todo: Make into a parameter.
    skidBufferMax = (decodeToRenameDelay + 1) * params.decodeWidth;
    for (uint32_t tid = 0; tid < Impl::MaxThreads; tid++) {
        renameStatus[tid] = Idle;
        renameMap[tid] = nullptr;
        instsInProgress[tid] = 0;
        loadsInProgress[tid] = 0;
        storesInProgress[tid] = 0;
        freeEntries[tid] = {0, 0, 0, 0};
        emptyROB[tid] = true;
        stalls[tid] = {false, false};
        serializeInst[tid] = nullptr;
        serializeOnNextInst[tid] = false;
    }
}

template <class Impl>
std::string
DefaultRename<Impl>::name() const
{
    return cpu->name() + ".rename";
}

template <class Impl>
DefaultRename<Impl>::RenameStats::RenameStats(Stats::Group *parent)
    : Stats::Group(parent, "rename"),
      ADD_STAT(squashCycles, "Number of cycles rename is squashing"),
      ADD_STAT(idleCycles, "Number of cycles rename is idle"),
      ADD_STAT(blockCycles, "Number of cycles rename is blocking"),
      ADD_STAT(serializeStallCycles, "count of cycles rename stalled"
          "for serializing inst"),
      ADD_STAT(runCycles, "Number of cycles rename is running"),
      ADD_STAT(unblockCycles, "Number of cycles rename is unblocking"),
      ADD_STAT(renamedInsts, "Number of instructions processed by"
          " rename"),
      ADD_STAT(squashedInsts, "Number of squashed instructions"
          " processed by rename"),
      ADD_STAT(ROBFullEvents, "Number of times rename has blocked"
          " due to ROB full"),
      ADD_STAT(IQFullEvents, "Number of times rename has blocked due"
          " to IQ full"),
      ADD_STAT(LQFullEvents, "Number of times rename has blocked due"
          " to LQ full" ),
      ADD_STAT(SQFullEvents, "Number of times rename has blocked due"
          " to SQ full"),
      ADD_STAT(fullRegistersEvents, "Number of times there has been no"
          " free registers"),
      ADD_STAT(renamedOperands, "Number of destination operands rename"
          " has renamed"),
      ADD_STAT(lookups, "Number of register rename lookups that"
          " rename has made"),
      ADD_STAT(intLookups, "Number of integer rename lookups"),
      ADD_STAT(fpLookups, "Number of floating rename lookups"),
      ADD_STAT(vecLookups, "Number of vector rename lookups"),
      ADD_STAT(vecPredLookups, "Number of vector predicate rename"
          " lookups"),
      ADD_STAT(renamedZeroIdioms, "Number of renamed zero idioms insts"),
      ADD_STAT(renamedOneIdioms, "Number of renamed one idioms insts"),
      ADD_STAT(renamedZeroVP, "Number of renamed zero vp insts"),
      ADD_STAT(renamedOneVP, "Number of renamed one vp insts"),
      ADD_STAT(renamedMoves, "Number of renamed move insts"),
      ADD_STAT(skippedMoveElim,
        "Number of moves not eliminated due to width mismatch"),
      ADD_STAT(numStrReducedToMove, "Number of insts dyn str"
       " reduced to move"),
      ADD_STAT(numStrReducedToMoveAdd, "Dyn add to move"),
      ADD_STAT(numStrReducedToMoveSub, "Dyn sub to move"),
      ADD_STAT(numStrReducedToMoveOrr, "Dyn orr to move"),
      ADD_STAT(numStrReducedToMoveXor, "Dyn eor to move"),
      ADD_STAT(numStrReducedToMoveShl, "Dyn shl to move"),
      ADD_STAT(numStrReducedToMoveShr, "Dyn shr to move"),
      ADD_STAT(numStrReducedToMoveMul, "Dyn mul to move"),
      ADD_STAT(numStrReducedToMoveDiv, "Dyn div to move"),
      ADD_STAT(numStrReducedToMoveCsel, "Dyn csel to move"),
      ADD_STAT(numStrReducedToMoveCsinc, "Dyn csinc to move"),
      ADD_STAT(numStrReducedToMoveCsneg, "Dyn csneg to move"),
      ADD_STAT(numStrReducedToMoveBic, "Dyn bic to move"),
      ADD_STAT(numStrReducedToZeroIdiom, "Number of insts dyn str"
       " reduced to zero idiom"),
      ADD_STAT(numStrReducedToZeroIdiomSub, "sub to zero idiom"),
      ADD_STAT(numStrReducedToZeroIdiomShl, "shl to zero idiom"),
      ADD_STAT(numStrReducedToZeroIdiomShr, "shr to zero idiom"),
      ADD_STAT(numStrReducedToZeroIdiomUbfm, "ubfm to zero idiom"),
      ADD_STAT(numStrReducedToZeroIdiomBic, "bic to zero idiom"),
      ADD_STAT(numStrReducedToZeroIdiomRbit, "rbit to zero idiom"),
      ADD_STAT(numStrReducedToZeroIdiomCsel, "csel to zero idiom"),
      ADD_STAT(numStrReducedToZeroIdiomCsinc, "csinc to zero idiom"),
      ADD_STAT(numStrReducedToZeroIdiomMul, "mul to zero idiom"),
      ADD_STAT(numStrReducedToZeroIdiomDiv, "div to zero idiom"),
      ADD_STAT(numStrReducedToOneIdiom, "Number of insts dyn str"
       " reduced to one idiom"),
      ADD_STAT(numStrReducedToOneIdiomAdd, "add to one idiom"),
      ADD_STAT(numStrReducedToOneIdiomOrr, "orr to one idiom"),
      ADD_STAT(numStrReducedToOneIdiomXor, "eor to one idiom"),
      ADD_STAT(numStrReducedToOneIdiomAnd, "and to one idiom"),
      ADD_STAT(numStrReducedToOneIdiomCsel, "csel to one idiom"),
      ADD_STAT(numStrReducedToOneIdiomCsinc, "csinc to one idiom"),
      ADD_STAT(numStrReducedToNop, "Number of insts dyn str"
       " reduced to nop"),
      ADD_STAT(numStrReducedToNopCC, "Number of CC dyn str "
       " reduced to nop"),
      ADD_STAT(numStrReducedToNopAnds, "Number of ands to nop"),
      ADD_STAT(numStrReducedToNopSubs, "Number of subs to nop"),
      ADD_STAT(numStrReducedToNopAdds, "Number of adds to nop"),
      ADD_STAT(numStrReducedToNopCbz, "Number of cbz/tbz dyn str "
       " reduced to nop"),
      ADD_STAT(numStrReducedToNopBCC, "Number of cc branches dyn str "
       " reduced to nop"),
      ADD_STAT(numEarlyBrMispred, "Number of br mispred"
       " early detected through DSR"),
      ADD_STAT(numBranchZeroPred, "Number of cbnz/cbz that led to"
       " register being predicted as 0"),
      ADD_STAT(numRenamedSrcBPred, "Number of insts that got"
      "zero reg as src due to cbnz/cbz zero pred"),
      ADD_STAT(numRenamedSubsShortImm, "Number of subs where one"
      " operand is a short int"),
      ADD_STAT(numShortImmBranchPred, "Number of b.eq that "
      " led to a value prediction"),
      ADD_STAT(committedMaps, "Number of HB maps that are committed"),
      ADD_STAT(undoneMaps, "Number of HB maps that are undone due to"
          " squashing"),
      ADD_STAT(serializing, "count of serializing insts renamed" ),
      ADD_STAT(tempSerializing, "count of temporary serializing insts"
          " renamed"),
      ADD_STAT(skidInsts, "count of insts added to the skid buffer")
{
    squashCycles.prereq(squashCycles);
    idleCycles.prereq(idleCycles);
    blockCycles.prereq(blockCycles);
    serializeStallCycles.flags(Stats::total);
    runCycles.prereq(idleCycles);
    unblockCycles.prereq(unblockCycles);

    renamedInsts.prereq(renamedInsts);
    squashedInsts.prereq(squashedInsts);

    ROBFullEvents.prereq(ROBFullEvents);
    IQFullEvents.prereq(IQFullEvents);
    LQFullEvents.prereq(LQFullEvents);
    SQFullEvents.prereq(SQFullEvents);
    fullRegistersEvents.prereq(fullRegistersEvents);

    renamedOperands.prereq(renamedOperands);
    lookups.prereq(lookups);
    intLookups.prereq(intLookups);
    fpLookups.prereq(fpLookups);
    vecLookups.prereq(vecLookups);
    vecPredLookups.prereq(vecPredLookups);

    committedMaps.prereq(committedMaps);
    undoneMaps.prereq(undoneMaps);
    serializing.flags(Stats::total);
    tempSerializing.flags(Stats::total);
    skidInsts.flags(Stats::total);
}

template <class Impl>
void
DefaultRename<Impl>::regProbePoints()
{
    ppRename = new ProbePointArg<DynInstPtr>(cpu->getProbeManager(), "Rename");
    ppSquashInRename = new ProbePointArg<SeqNumRegPair>(cpu->getProbeManager(),
                                                        "SquashInRename");
}

template <class Impl>
void
DefaultRename<Impl>::setTimeBuffer(TimeBuffer<TimeStruct> *tb_ptr)
{
    timeBuffer = tb_ptr;

    // Setup wire to read information from time buffer, from IEW stage.
    fromIEW = timeBuffer->getWire(-iewToRenameDelay);

    // Setup wire to read infromation from time buffer, from commit stage.
    fromCommit = timeBuffer->getWire(-commitToRenameDelay);

    // Setup wire to write information to previous stages.
    toDecode = timeBuffer->getWire(0);
}

template <class Impl>
void
DefaultRename<Impl>::setRenameQueue(TimeBuffer<RenameStruct> *rq_ptr)
{
    renameQueue = rq_ptr;

    // Setup wire to write information to future stages.
    toIEW = renameQueue->getWire(0);
}

template <class Impl>
void
DefaultRename<Impl>::setDecodeQueue(TimeBuffer<DecodeStruct> *dq_ptr)
{
    decodeQueue = dq_ptr;

    // Setup wire to get information from decode.
    fromDecode = decodeQueue->getWire(-decodeToRenameDelay);
}

template <class Impl>
void
DefaultRename<Impl>::startupStage()
{
    resetStage();
}

template <class Impl>
void
DefaultRename<Impl>::clearStates(ThreadID tid)
{
    renameStatus[tid] = Idle;

    freeEntries[tid].iqEntries = iew_ptr->instQueue.numFreeEntries(tid);
    freeEntries[tid].lqEntries = iew_ptr->ldstQueue.numFreeLoadEntries(tid);
    freeEntries[tid].sqEntries = iew_ptr->ldstQueue.numFreeStoreEntries(tid);
    freeEntries[tid].robEntries = commit_ptr->numROBFreeEntries(tid);
    emptyROB[tid] = true;

    stalls[tid].iew = false;
    serializeInst[tid] = NULL;

    instsInProgress[tid] = 0;
    loadsInProgress[tid] = 0;
    storesInProgress[tid] = 0;

    serializeOnNextInst[tid] = false;
}

template <class Impl>
void
DefaultRename<Impl>::resetStage()
{
    _status = Inactive;

    resumeSerialize = false;
    resumeUnblocking = false;

    // Grab the number of free entries directly from the stages.
    for (ThreadID tid = 0; tid < numThreads; tid++) {
        renameStatus[tid] = Idle;

        freeEntries[tid].iqEntries = iew_ptr->instQueue.numFreeEntries(tid);
        freeEntries[tid].lqEntries = iew_ptr->ldstQueue.numFreeLoadEntries(tid);
        freeEntries[tid].sqEntries = iew_ptr->ldstQueue.numFreeStoreEntries(tid);
        freeEntries[tid].robEntries = commit_ptr->numROBFreeEntries(tid);
        emptyROB[tid] = true;

        stalls[tid].iew = false;
        serializeInst[tid] = NULL;

        instsInProgress[tid] = 0;
        loadsInProgress[tid] = 0;
        storesInProgress[tid] = 0;

        serializeOnNextInst[tid] = false;
    }
}

template<class Impl>
void
DefaultRename<Impl>::setActiveThreads(list<ThreadID> *at_ptr)
{
    activeThreads = at_ptr;
}


template <class Impl>
void
DefaultRename<Impl>::setRenameMap(RenameMap rm_ptr[])
{
    for (ThreadID tid = 0; tid < numThreads; tid++)
        renameMap[tid] = &rm_ptr[tid];
}

template <class Impl>
void
DefaultRename<Impl>::setFreeList(FreeList *fl_ptr)
{
    freeList = fl_ptr;

    // Hardwired 0x0 and 0x1 registers are always ready.
    scoreboard->setReg(freeList->hardwiredPhyReg(0x0));
    if (enableRenameIdioms)
    {
        scoreboard->setReg(freeList->hardwiredPhyReg(0x1));
    }
}

template<class Impl>
void
DefaultRename<Impl>::setScoreboard(Scoreboard *_scoreboard)
{
    scoreboard = _scoreboard;
}

template <class Impl>
bool
DefaultRename<Impl>::isDrained() const
{
    for (ThreadID tid = 0; tid < numThreads; tid++) {
        if (instsInProgress[tid] != 0 ||
            !historyBuffer[tid].empty() ||
            !skidBuffer[tid].empty() ||
            !insts[tid].empty() ||
            (renameStatus[tid] != Idle && renameStatus[tid] != Running))
            return false;
    }
    return true;
}

template <class Impl>
void
DefaultRename<Impl>::takeOverFrom()
{
    resetStage();
}

template <class Impl>
void
DefaultRename<Impl>::drainSanityCheck() const
{
    for (ThreadID tid = 0; tid < numThreads; tid++) {
        assert(historyBuffer[tid].empty());
        assert(insts[tid].empty());
        assert(skidBuffer[tid].empty());
        assert(instsInProgress[tid] == 0);
    }
}

template <class Impl>
void
DefaultRename<Impl>::squash(const InstSeqNum &squash_seq_num, ThreadID tid)
{
    DPRINTF(Rename, "[tid:%i] [squash sn:%llu] Squashing instructions.\n",
        tid,squash_seq_num);

    // Clear the stall signal if rename was blocked or unblocking before.
    // If it still needs to block, the blocking should happen the next
    // cycle and there should be space to hold everything due to the squash.
    if (renameStatus[tid] == Blocked ||
        renameStatus[tid] == Unblocking) {
        toDecode->renameUnblock[tid] = 1;

        resumeSerialize = false;
        serializeInst[tid] = NULL;
    } else if (renameStatus[tid] == SerializeStall) {
        if (serializeInst[tid]->seqNum <= squash_seq_num) {
            DPRINTF(Rename, "[tid:%i] [squash sn:%llu] "
                "Rename will resume serializing after squash\n",
                tid,squash_seq_num);
            resumeSerialize = true;
            assert(serializeInst[tid]);
        } else {
            resumeSerialize = false;
            toDecode->renameUnblock[tid] = 1;

            serializeInst[tid] = NULL;
        }
    }

    // Set the status to Squashing.
    renameStatus[tid] = Squashing;

    // Squash any instructions from decode.
    for (int i=0; i<fromDecode->size; i++) {
        if (fromDecode->insts[i]->threadNumber == tid &&
            fromDecode->insts[i]->seqNum > squash_seq_num) {
            fromDecode->insts[i]->setSquashed();
            wroteToTimeBuffer = true;
        }

    }

    // Clear the instruction list and skid buffer in case they have any
    // insts in them.
    insts[tid].clear();

    // Clear the skid buffer in case it has any data in it.
    skidBuffer[tid].clear();

    doSquash(squash_seq_num, tid);
}

template <class Impl>
void
DefaultRename<Impl>::tick()
{
    wroteToTimeBuffer = false;

    blockThisCycle = false;

    bool status_change = false;

    toIEWIndex = 0;

    sortInsts();

    list<ThreadID>::iterator threads = activeThreads->begin();
    list<ThreadID>::iterator end = activeThreads->end();

    // Check stall and squash signals.
    while (threads != end) {
        ThreadID tid = *threads++;

        DPRINTF(Rename, "Processing [tid:%i]\n", tid);

        status_change = checkSignalsAndUpdate(tid) || status_change;

        rename(status_change, tid);
    }

    if (status_change) {
        updateStatus();
    }

    if (wroteToTimeBuffer) {
        DPRINTF(Activity, "Activity this cycle.\n");
        cpu->activityThisCycle();
    }

    threads = activeThreads->begin();

    while (threads != end) {
        ThreadID tid = *threads++;

        // If we committed this cycle then doneSeqNum will be > 0
        if (fromCommit->commitInfo[tid].doneSeqNum != 0 &&
            !fromCommit->commitInfo[tid].squash &&
            renameStatus[tid] != Squashing) {

            removeFromHistory(fromCommit->commitInfo[tid].doneSeqNum,
                                  tid);
        }
    }

    // @todo: make into updateProgress function
    for (ThreadID tid = 0; tid < numThreads; tid++) {
        instsInProgress[tid] -= fromIEW->iewInfo[tid].dispatched;
        loadsInProgress[tid] -= fromIEW->iewInfo[tid].dispatchedToLQ;
        storesInProgress[tid] -= fromIEW->iewInfo[tid].dispatchedToSQ;
        assert(loadsInProgress[tid] >= 0);
        assert(storesInProgress[tid] >= 0);
        assert(instsInProgress[tid] >=0);
    }

}

template<class Impl>
void
DefaultRename<Impl>::rename(bool &status_change, ThreadID tid)
{
    // If status is Running or idle,
    //     call renameInsts()
    // If status is Unblocking,
    //     buffer any instructions coming from decode
    //     continue trying to empty skid buffer
    //     check if stall conditions have passed

    if (renameStatus[tid] == Blocked) {
        ++stats.blockCycles;
    } else if (renameStatus[tid] == Squashing) {
        ++stats.squashCycles;
    } else if (renameStatus[tid] == SerializeStall) {
        ++stats.serializeStallCycles;
        // If we are currently in SerializeStall and resumeSerialize
        // was set, then that means that we are resuming serializing
        // this cycle.  Tell the previous stages to block.
        if (resumeSerialize) {
            resumeSerialize = false;
            block(tid);
            toDecode->renameUnblock[tid] = false;
        }
    } else if (renameStatus[tid] == Unblocking) {
        if (resumeUnblocking) {
            block(tid);
            resumeUnblocking = false;
            toDecode->renameUnblock[tid] = false;
        }
    }

    if (renameStatus[tid] == Running ||
        renameStatus[tid] == Idle) {
        DPRINTF(Rename,
                "[tid:%i] "
                "Not blocked, so attempting to run stage.\n",
                tid);

        renameInsts(tid);
    } else if (renameStatus[tid] == Unblocking) {
        renameInsts(tid);

        if (validInsts()) {
            // Add the current inputs to the skid buffer so they can be
            // reprocessed when this stage unblocks.
            skidInsert(tid);
        }

        // If we switched over to blocking, then there's a potential for
        // an overall status change.
        status_change = unblock(tid) || status_change || blockThisCycle;
    }
}

template <class Impl>
void
DefaultRename<Impl>::renameInsts(ThreadID tid)
{
    // Instructions can be either in the skid buffer or the queue of
    // instructions coming from decode, depending on the status.
    int insts_available = renameStatus[tid] == Unblocking ?
        skidBuffer[tid].size() : insts[tid].size();

    int initially_available = insts_available;

    // Check the decode queue to see if instructions are available.
    // If there are no available instructions to rename, then do nothing.
    if (insts_available == 0) {
        DPRINTF(Rename, "[tid:%i] Nothing to do, breaking out early.\n",
                tid);
        // Should I change status to idle?
        ++stats.idleCycles;
        return;
    } else if (renameStatus[tid] == Unblocking) {
        ++stats.unblockCycles;
    } else if (renameStatus[tid] == Running) {
        ++stats.runCycles;
    }

    // Will have to do a different calculation for the number of free
    // entries.
    int free_rob_entries = calcFreeROBEntries(tid);
    int free_iq_entries  = calcFreeIQEntries(tid);
    int min_free_entries = free_rob_entries;

    int rob_used = 0;
    int iq_used = 0;

    FullSource source = ROB;

    if (free_iq_entries < min_free_entries) {
        min_free_entries = free_iq_entries;
        source = IQ;
    }

    // Check if there's any space left.
    if (min_free_entries <= 0) {
        DPRINTF(Rename,
                "[tid:%i] Blocking due to no free ROB/IQ/ entries.\n"
                "ROB has %i free entries.\n"
                "IQ has %i free entries.\n",
                tid, free_rob_entries, free_iq_entries);

        blockThisCycle = true;

        block(tid);

        incrFullStat(source);

        return;
    } else if (min_free_entries < insts_available) {
        DPRINTF(Rename,
                "[tid:%i] "
                "Will have to block this cycle. "
                "%i insts available, "
                "but only %i insts can be renamed due to ROB/IQ/LSQ limits.\n",
                tid, insts_available, min_free_entries);

        insts_available = min_free_entries;

        blockThisCycle = true;

        incrFullStat(source);
    }

    InstQueue &insts_to_rename = renameStatus[tid] == Unblocking ?
        skidBuffer[tid] : insts[tid];

    DPRINTF(Rename,
            "[tid:%i] "
            "%i available instructions to send iew.\n",
            tid, insts_available);

    DPRINTF(Rename,
            "[tid:%i] "
            "%i insts pipelining from Rename | "
            "%i insts dispatched to IQ last cycle.\n",
            tid, instsInProgress[tid], fromIEW->iewInfo[tid].dispatched);

    // Handle serializing the next instruction if necessary.
    if (serializeOnNextInst[tid]) {
        if (emptyROB[tid] && instsInProgress[tid] == 0) {
            // ROB already empty; no need to serialize.
            serializeOnNextInst[tid] = false;
        } else if (!insts_to_rename.empty()) {
            insts_to_rename.front()->setSerializeBefore();
        }
    }

    int renamed_insts = 0;

    while (insts_available > 0 &&  toIEWIndex < renameWidth) {
        DPRINTF(Rename, "[tid:%i] Sending instructions to IEW.\n", tid);

        assert(!insts_to_rename.empty());

        DynInstPtr inst = insts_to_rename.front();

        //For all kind of instructions, check ROB and IQ first
        //For load instruction, check LQ size and take into account the inflight loads
        //For store instruction, check SQ size and take into account the inflight stores

        if (inst->isLoad()) {
            if (calcFreeLQEntries(tid) <= 0) {
                DPRINTF(Rename, "[tid:%i] Cannot rename due to no free LQ\n");
                source = LQ;
                incrFullStat(source);
                break;
            }
        }

        if (inst->isStore() || inst->isAtomic()) {
            if (calcFreeSQEntries(tid) <= 0) {
                DPRINTF(Rename, "[tid:%i] Cannot rename due to no free SQ\n");
                source = SQ;
                incrFullStat(source);
                break;
            }
        }

        insts_to_rename.pop_front();

        if (renameStatus[tid] == Unblocking) {
            DPRINTF(Rename,
                    "[tid:%i] "
                    "Removing [sn:%llu] PC:%s from rename skidBuffer\n",
                    tid, inst->seqNum, inst->pcState());
        }

        if (inst->isSquashed()) {
            DPRINTF(Rename,
                    "[tid:%i] "
                    "instruction %i with PC %s is squashed, skipping.\n",
                    tid, inst->seqNum, inst->pcState());

            ++stats.squashedInsts;

            // Decrement how many instructions are available.
            --insts_available;
            --initially_available;

            continue;
        }

        DPRINTF(Rename,
                "[tid:%i] "
                "Processing instruction [sn:%llu] with PC %s.\n",
                tid, inst->seqNum, inst->pcState());

        renameSrcRegs(inst, inst->threadNumber);

        if (inst->staticInst->isCC())
        {
            nzcv_available = false;
            left = nullptr;
            right = nullptr;
            imm = -1;
            nzcv_seq_num = inst->seqNum;
            valid_last_nzcv_prod = true;

            if (inst->staticInst->isSub())
            {
                left = inst->renamedSrcRegIdx(0);
                left_arch = inst->srcRegIdx(0);

                if (inst->numSrcRegs() == 2)
                {
                    right = inst->renamedSrcRegIdx(1);
                    imm = -1;
                }
                else
                {
                    // Start with subs that compares two regs for now
                    std::string dis = inst->staticInst->disassemble(
                        inst->pcState().instAddr());

                    assert (inst->numSrcRegs() == 1);
                    right = nullptr;

                    // Extracting the immediate by hand...
                    auto start = dis.begin() + dis.find("#") + 1;
                    auto end = start;
                    for (; end != dis.end(); end++)
                    {
                        if (*end == ' ')
                        {
                            break;
                        }
                    }
                    imm = stoi(std::string(start, end));
                    stats.numRenamedSubsShortImm += imm >= -256 && imm <= 255;
                }
            }
        }

        bool can_move_elim = enableDynStrReduction &&
            canStrReduceToMove(inst);
        const bool can_one_idiom = enableDynStrReduction &&
            canStrReduceToOneIdiom(inst);
        const bool can_zero_idiom = enableDynStrReduction &&
            canStrReduceToZeroIdiom(inst);
        const bool can_nop_in_place = enableDynStrReduction &&
            canStrReduceToNop(inst);

        bool early_redirect = false;

        if (inst->isValuePredicted() && can_move_elim)
        {
            can_move_elim = false;
            inst->setNotDynMoveElimVP();
        }
        // Note : DynMvElim loses to VP
        if (can_move_elim && !can_one_idiom &&
            !can_zero_idiom && !can_nop_in_place)
        {
            inst->setDynMove();

            ++stats.numStrReducedToMove;
            stats.numStrReducedToMoveAdd += inst->staticInst->isAdd();
            stats.numStrReducedToMoveSub += inst->staticInst->isSub();
            stats.numStrReducedToMoveOrr += inst->staticInst->isOr();
            stats.numStrReducedToMoveXor += inst->staticInst->isXor();
            stats.numStrReducedToMoveShl += inst->staticInst->isShl();
            stats.numStrReducedToMoveShr += inst->staticInst->isShr();
            stats.numStrReducedToMoveMul += inst->staticInst->isMul();
            stats.numStrReducedToMoveDiv += inst->staticInst->isDiv();
            stats.numStrReducedToMoveCsel += inst->staticInst->isCsel();
            stats.numStrReducedToMoveCsinc += inst->staticInst->isCsinc();
            stats.numStrReducedToMoveCsneg += inst->staticInst->isCsneg();
            stats.numStrReducedToMoveBic += inst->staticInst->isBic();

            DPRINTF(Rename, "[sn:%lu] Dynamic move\n", inst->seqNum);
        }
        if (can_one_idiom)
        {
            can_move_elim = false;
            inst->setDynOneIdiom();

            if (inst->isValuePredicted())
            {
                inst->setClearVPCauseIdiom();
            }
            inst->clearValuePredicted();

            ++stats.numStrReducedToOneIdiom;
            stats.numStrReducedToOneIdiomAdd += inst->staticInst->isAdd();
            stats.numStrReducedToOneIdiomOrr += inst->staticInst->isOr();
            stats.numStrReducedToOneIdiomXor += inst->staticInst->isXor();
            stats.numStrReducedToOneIdiomAnd += inst->staticInst->isAnd();
            stats.numStrReducedToOneIdiomCsel += inst->staticInst->isCsel();
            stats.numStrReducedToOneIdiomCsinc += inst->staticInst->isCsinc();

            DPRINTF(Rename, "[sn:%lu] Dynamic one idiom\n", inst->seqNum);
        }
        if (can_zero_idiom)
        {
            can_move_elim = false;
            inst->setDynZeroIdiom();
            if (inst->isValuePredicted())
            {
                inst->setClearVPCauseIdiom();
            }
            inst->clearValuePredicted();

            ++stats.numStrReducedToZeroIdiom;
            stats.numStrReducedToZeroIdiomSub += inst->staticInst->isSub();
            stats.numStrReducedToZeroIdiomShl += inst->staticInst->isShl();
            stats.numStrReducedToZeroIdiomShr += inst->staticInst->isShr();
            stats.numStrReducedToZeroIdiomUbfm += inst->staticInst->isUbfm();
            stats.numStrReducedToZeroIdiomBic += inst->staticInst->isBic();
            stats.numStrReducedToZeroIdiomRbit += inst->staticInst->isRbit();
            stats.numStrReducedToZeroIdiomCsel += inst->staticInst->isCsel();
            stats.numStrReducedToZeroIdiomCsinc += inst->staticInst->isCsinc();
            stats.numStrReducedToZeroIdiomMul += inst->staticInst->isMul();
            stats.numStrReducedToZeroIdiomDiv += inst->staticInst->isDiv();

            DPRINTF(Rename, "[sn:%lu] Dynamic zero idiom\n", inst->seqNum);
        }

        const bool is_zero_idiom = enableRenameIdioms &&
          (inst->staticInst->isZeroIdiom() || inst->isDynZeroIdiom());
        const bool is_one_idiom = enableRenameIdioms &&
          (inst->staticInst->isOneIdiom() || inst->isDynOneIdiom());

        const bool small_const = enableSmallConsts &&
            inst->staticInst->isSmallConst();

        const bool can_pot_move_elim = enableMoveElimination &&
            (inst->staticInst->isMove() ||
            inst->isDynMove()) &&
            !can_zero_idiom &&
            !can_one_idiom &&
            !can_nop_in_place;

        const bool all_regs_vp = enableSmallConsts &&
            inst->allDestNoRegs();

        const bool do_not_need_reg = is_zero_idiom |
            is_one_idiom |
            can_pot_move_elim |
            small_const |
            all_regs_vp;

        // Check here to make sure there are enough destination registers
        // to rename to.  Otherwise block.
        if (!do_not_need_reg &&
            !renameMap[tid]->canRename(inst->numIntDestRegs(),
                                       inst->numFPDestRegs(),
                                       inst->numVecDestRegs(),
                                       inst->numVecElemDestRegs(),
                                       inst->numVecPredDestRegs(),
                                       inst->numCCDestRegs())) {
            DPRINTF(Rename,
                    "Blocking due to "
                    " lack of free physical registers to rename to.\n");
            blockThisCycle = true;
            insts_to_rename.push_front(inst);
            ++stats.fullRegistersEvents;

            // Inst will go through src rename again
            if (inst->numSrcRegs() != 0)
            {
                inst->clearAllSrcRegsReady();
            }

            break;
        }

        // Handle serializeAfter/serializeBefore instructions.
        // serializeAfter marks the next instruction as serializeBefore.
        // serializeBefore makes the instruction wait in rename until the ROB
        // is empty.

        // In this model, IPR accesses are serialize before
        // instructions, and store conditionals are serialize after
        // instructions.  This is mainly due to lack of support for
        // out-of-order operations of either of those classes of
        // instructions.
        if (inst->isSerializeBefore() && !inst->isSerializeHandled()) {
            DPRINTF(Rename, "Serialize before instruction encountered.\n");

            if (!inst->isTempSerializeBefore()) {
                stats.serializing++;
                inst->setSerializeHandled();
            } else {
                stats.tempSerializing++;
            }

            // Change status over to SerializeStall so that other stages know
            // what this is blocked on.
            renameStatus[tid] = SerializeStall;

            serializeInst[tid] = inst;

            blockThisCycle = true;

            break;
        } else if ((inst->isStoreConditional() || inst->isSerializeAfter()) &&
                   !inst->isSerializeHandled()) {
            DPRINTF(Rename, "Serialize after instruction encountered.\n");

            stats.serializing++;

            inst->setSerializeHandled();

            serializeAfter(insts_to_rename, tid);
        }

        if (!renameDestRegs(inst, inst->threadNumber))
        {
            DPRINTF(Rename,
                    "Blocking due to "
                    " lack of free physical registers to rename to.\n");
            blockThisCycle = true;
            insts_to_rename.push_front(inst);
            ++stats.fullRegistersEvents;

            // Inst will go through src rename again
            if (inst->numSrcRegs() != 0)
            {
                inst->clearAllSrcRegsReady();
            }

            break;
        }

        // Actually need to know dest regs for ARM op-s (ands, subs, etc.)
        if (can_nop_in_place)
        {
            // CC-writer reduction
            stats.numStrReducedToNopCC += inst->staticInst->isCC();
            stats.numStrReducedToNopAnds +=
                inst->staticInst->isCC() && inst->staticInst->isAnd();
            stats.numStrReducedToNopSubs +=
                inst->staticInst->isCC() && inst->staticInst->isSub();
            stats.numStrReducedToNopAdds +=
                inst->staticInst->isCC() && inst->staticInst->isAdd();

            // If this is a branch, we'll know if it mispredicted here,
            // however, misprediction may be due to value prediction so
            // do we squash now ?
            if (enableEarlyBranchResolution || !inst->staticInst->isControl())
            {
                ++stats.numStrReducedToNop;

                DPRINTF(Rename, "[sn:%lu] Dynamic nop\n", inst->seqNum);
                inst->setDynNop();
                inst->execute();

                if (inst->isCondCtrl())
                {
                    cpu->fetch.resolveBranch(inst->seqNum);
                }

                const bool cbz_tbz = inst->staticInst->isCbz() |
                    inst->staticInst->isCbnz() |
                    inst->staticInst->isTbz();

                // Reg-based Cond Branch reduction
                stats.numStrReducedToNopCbz += cbz_tbz;

                 // CC-reader cond branch reduction
                stats.numStrReducedToNopBCC +=  !cbz_tbz &
                    inst->staticInst->isCondCtrl();

                // Adjust CPU intReg count : we did not really read
                // the int reg...
                // Beware, if src is hardwired, it will already be ignored.
                // Will adapt the number of elidedReads in inst_queue_impl.hh
                for (int i = 0; i < inst->numSrcRegs(); i++)
                {
                    cpu->cpuStats.intRegfileReads -=
                        inst->renamedSrcRegIdx(i)->isIntPhysReg() &&
                        (inst->renamedSrcRegIdx(i)->index() > 1 &&
                        !scoreboard->getRegVP(inst->renamedSrcRegIdx(i)));
                }

                for (int i = 0; i < inst->numDestRegs(); i++)
                {
                    scoreboard->setReg(inst->renamedDestRegIdx(i));
                }
            }

            nzcv_available |= inst->staticInst->isCC();
            nzcv_seq_num = inst->seqNum;

            if (enableEarlyBranchResolution &&
                inst->staticInst->isControl() &&
                inst->mispredicted())
            {
              // Do it before calling decode.squash
              // Since this is needed to compute the
              // correct direction
              inst->setEarlyCorrectedBranch();
              ++stats.numEarlyBrMispred;
              assert(!fromCommit->commitInfo[tid].squash);
              cpu->decode.squash(inst, tid);
              squash(inst->seqNum, tid);
              // This will break if we NOP something with
              // a dest reg
              early_redirect = true;

            }
        }

// Let's put a pin in that for now
#ifdef ENABLE_SILENT_STORE_DETECTION
        if (inst->isLoad() &&
            inst->numSrcRegs() == 1 &&
            inst->numDestRegs() == 1)
        {
            assert(inst->srcRegIdx(0).index() < 128);
            renamed_mem_reg.at(inst->srcRegIdx(0).index()) =
                MemRename{inst->renamedSrcRegIdx(0)->index(),
                inst->renamedDestRegIdx(0)->index()};
        }
        else if (inst->isStore() &&
            inst->numSrcRegs() == 2)
        {
            assert(inst->srcRegIdx(0).index() < 128);
            // If we are storing through arch reg X
            const auto & tracker =
                renamed_mem_reg.at(inst->srcRegIdx(0).index());

            // We are just storing the same phy reg that was loaded and
            // The base arch reg that ldr used has not changed (is still
            // same preg)
            const bool silent_store =
                (inst->renamedSrcRegIdx(1)->index() == tracker.load_phy_reg) &
                (inst->renamedSrcRegIdx(0)->index() == tracker.base_phy_reg);

            if (silent_store)
            {
                inst->printSmall();
                std::cout << "Intéréssant" << std::endl;
            }
        }
        else if (inst->numDestRegs() != 0)
        {
            for (int i = 0; i < inst->numDestRegs(); i++)
            {
                if (inst->destRegIdx(i).isIntReg())
                {
                    renamed_mem_reg.at(inst->destRegIdx(i).index()) =
                        MemRename{-1, -1};
                }
            }
        }
#endif

        if (inst->isAtomic() || inst->isStore()) {
            storesInProgress[tid]++;
        } else if (inst->isLoad()) {
            loadsInProgress[tid]++;
        }

        ++renamed_insts;
        // Notify potential listeners that source and destination registers for
        // this instruction have been renamed.
        ppRename->notify(inst);

        // Put instruction in rename queue.
        toIEW->insts[toIEWIndex] = inst;
        ++(toIEW->size);

        // Increment which instruction we're on.
        ++toIEWIndex;

        // Decrement how many instructions are available.
        --insts_available;
        --initially_available;

        // Resolve whether it consumed an IQ or not
        const bool is_move = enableMoveElimination &&
          inst->isMoveEliminated();
        const bool is_nop = inst->isDynNop() ||
            inst->isNop() || inst->staticInst->isPrefetch();

        const bool elim = is_zero_idiom |
          is_one_idiom |
          small_const |
          is_move |
          is_nop ;

        rob_used++;
        iq_used += !elim;

        DPRINTF(Rename, "Avail: %lu, iavail: %lu\n",
            insts_available,
            initially_available);

        if (source == IQ && (initially_available != 0))
        {
          insts_available = std::min((int) insts_available + elim,
            (int) insts_to_rename.size());
          // Is source still IQ ? (Can be ROB or regs)
          if (rob_used == free_rob_entries)
          {
            source = ROB;
            insts_available = 0;
          }
        }

        if (early_redirect)
        {
            insts_available = 0;
            break;
        }
    }

    instsInProgress[tid] += renamed_insts;
    stats.renamedInsts += renamed_insts;

    // If we wrote to the time buffer, record this.
    if (toIEWIndex) {
        wroteToTimeBuffer = true;
    }

    // Check if there's any instructions left that haven't yet been renamed.
    // If so then block.
    if (insts_available) {
        blockThisCycle = true;
    }

    if (blockThisCycle) {
        block(tid);
        toDecode->renameUnblock[tid] = false;
    }
}

template<class Impl>
void
DefaultRename<Impl>::skidInsert(ThreadID tid)
{
    DynInstPtr inst = NULL;

    while (!insts[tid].empty()) {
        inst = insts[tid].front();

        insts[tid].pop_front();

        assert(tid == inst->threadNumber);

        DPRINTF(Rename, "[tid:%i] Inserting [sn:%llu] PC: %s into Rename "
                "skidBuffer\n", tid, inst->seqNum, inst->pcState());

        ++stats.skidInsts;

        skidBuffer[tid].push_back(inst);
    }

    if (skidBuffer[tid].size() > skidBufferMax)
    {
        typename InstQueue::iterator it;
        warn("Skidbuffer contents:\n");
        for (it = skidBuffer[tid].begin(); it != skidBuffer[tid].end(); it++)
        {
            warn("[tid:%i] %s [sn:%llu].\n", tid,
                    (*it)->staticInst->disassemble(inst->instAddr()),
                    (*it)->seqNum);
        }
        panic("Skidbuffer Exceeded Max Size");
    }
}

template <class Impl>
void
DefaultRename<Impl>::sortInsts()
{
    int insts_from_decode = fromDecode->size;
    for (int i = 0; i < insts_from_decode; ++i) {
        const DynInstPtr &inst = fromDecode->insts[i];
        insts[inst->threadNumber].push_back(inst);
#if TRACING_ON
        if (DTRACE(O3PipeView)) {
            inst->renameTick = curTick() - inst->fetchTick;
        }
#endif
    }
}

template<class Impl>
bool
DefaultRename<Impl>::skidsEmpty()
{
    list<ThreadID>::iterator threads = activeThreads->begin();
    list<ThreadID>::iterator end = activeThreads->end();

    while (threads != end) {
        ThreadID tid = *threads++;

        if (!skidBuffer[tid].empty())
            return false;
    }

    return true;
}

template<class Impl>
void
DefaultRename<Impl>::updateStatus()
{
    bool any_unblocking = false;

    list<ThreadID>::iterator threads = activeThreads->begin();
    list<ThreadID>::iterator end = activeThreads->end();

    while (threads != end) {
        ThreadID tid = *threads++;

        if (renameStatus[tid] == Unblocking) {
            any_unblocking = true;
            break;
        }
    }

    // Rename will have activity if it's unblocking.
    if (any_unblocking) {
        if (_status == Inactive) {
            _status = Active;

            DPRINTF(Activity, "Activating stage.\n");

            cpu->activateStage(O3CPU::RenameIdx);
        }
    } else {
        // If it's not unblocking, then rename will not have any internal
        // activity.  Switch it to inactive.
        if (_status == Active) {
            _status = Inactive;
            DPRINTF(Activity, "Deactivating stage.\n");

            cpu->deactivateStage(O3CPU::RenameIdx);
        }
    }
}

template <class Impl>
bool
DefaultRename<Impl>::block(ThreadID tid)
{
    DPRINTF(Rename, "[tid:%i] Blocking.\n", tid);

    // Add the current inputs onto the skid buffer, so they can be
    // reprocessed when this stage unblocks.
    skidInsert(tid);

    // Only signal backwards to block if the previous stages do not think
    // rename is already blocked.
    if (renameStatus[tid] != Blocked) {
        // If resumeUnblocking is set, we unblocked during the squash,
        // but now we're have unblocking status. We need to tell earlier
        // stages to block.
        if (resumeUnblocking || renameStatus[tid] != Unblocking) {
            toDecode->renameBlock[tid] = true;
            toDecode->renameUnblock[tid] = false;
            wroteToTimeBuffer = true;
        }

        // Rename can not go from SerializeStall to Blocked, otherwise
        // it would not know to complete the serialize stall.
        if (renameStatus[tid] != SerializeStall) {
            // Set status to Blocked.
            renameStatus[tid] = Blocked;
            return true;
        }
    }

    return false;
}

template <class Impl>
bool
DefaultRename<Impl>::unblock(ThreadID tid)
{
    DPRINTF(Rename, "[tid:%i] Trying to unblock.\n", tid);

    // Rename is done unblocking if the skid buffer is empty.
    if (skidBuffer[tid].empty() && renameStatus[tid] != SerializeStall) {

        DPRINTF(Rename, "[tid:%i] Done unblocking.\n", tid);

        toDecode->renameUnblock[tid] = true;
        wroteToTimeBuffer = true;

        renameStatus[tid] = Running;
        return true;
    }

    return false;
}

template <class Impl>
void
DefaultRename<Impl>::doSquash(const InstSeqNum &squashed_seq_num, ThreadID tid)
{
    typename std::list<RenameHistory>::iterator hb_it =
        historyBuffer[tid].begin();

#ifdef ENABLE_SILENT_STORE_DETECTION
    renamed_mem_reg.resize(128, {-1, -1});
#endif

    // After a syscall squashes everything, the history buffer may be empty
    // but the ROB may still be squashing instructions.
    // Go through the most recent instructions, undoing the mappings
    // they did and freeing up the registers.
    while (!historyBuffer[tid].empty() &&
           hb_it->instSeqNum > squashed_seq_num) {
        assert(hb_it != historyBuffer[tid].end());

        DPRINTF(Rename, "[tid:%i] Removing history entry with sequence "
                "number %i (archReg: %d, newPhysReg: %d, prevPhysReg: %d).\n",
                tid, hb_it->instSeqNum, hb_it->archReg.index(),
                hb_it->newPhysReg->index(), hb_it->prevPhysReg->index());

        // Undo the rename mapping only if it was really a change.
        // Special regs that are not really renamed (like misc regs
        // and the zero reg) can be recognized because the new mapping
        // is the same as the old one.  While it would be merely a
        // waste of time to update the rename table, we definitely
        // don't want to put these on the free list.
        const bool using_hardwired_reg =
            (hb_it->newPhysReg->classValue() == IntRegClass) &&
            freeList->isHardwiredReg(hb_it->newPhysReg);


        // Tell the rename map to set the architected register to the
        // previous physical register that it was renamed to.
        renameMap[tid]->setEntry(hb_it->archReg,
            hb_it->prevPhysReg,
            hb_it->prevExtended);

        // Put the renamed physical register back on the free list.
        if (!using_hardwired_reg &&
            ((hb_it->newPhysReg != hb_it->prevPhysReg) || hb_it->isMove))
        {
            freeList->addReg(hb_it->newPhysReg);
        }

        // Notify potential listeners that the register mapping needs to be
        // removed because the instruction it was mapped to got squashed. Note
        // that this is done before hb_it is incremented.
        ppSquashInRename->notify(std::make_pair(hb_it->instSeqNum,
                                                hb_it->newPhysReg));

        historyBuffer[tid].erase(hb_it++);

        ++stats.undoneMaps;
    }

    // Check if we need to change vector renaming mode after squashing
    cpu->switchRenameMode(tid, freeList);
}

template<class Impl>
void
DefaultRename<Impl>::removeFromHistory(InstSeqNum inst_seq_num, ThreadID tid)
{
    DPRINTF(Rename, "[tid:%i] Removing a committed instruction from the "
            "history buffer %u (size=%i), until [sn:%llu].\n",
            tid, tid, historyBuffer[tid].size(), inst_seq_num);

    typename std::list<RenameHistory>::iterator hb_it =
        historyBuffer[tid].end();

    --hb_it;

    if (historyBuffer[tid].empty()) {
        DPRINTF(Rename, "[tid:%i] History buffer is empty.\n", tid);
        return;
    } else if (hb_it->instSeqNum > inst_seq_num) {
        DPRINTF(Rename, "[tid:%i] [sn:%llu] "
                "Old sequence number encountered. "
                "Ensure that a syscall happened recently.\n",
                tid,inst_seq_num);
        return;
    }

    // Commit all the renames up until (and including) the committed sequence
    // number. Some or even all of the committed instructions may not have
    // rename histories if they did not have destination registers that were
    // renamed.
    while (!historyBuffer[tid].empty() &&
           hb_it != historyBuffer[tid].end() &&
           hb_it->instSeqNum <= inst_seq_num) {

        DPRINTF(Rename, "[tid:%i] Freeing up older rename of reg %i (%s), "
                "[sn:%llu].\n",
                tid, hb_it->prevPhysReg->index(),
                hb_it->prevPhysReg->className(),
                hb_it->instSeqNum);

        // Don't free special phys regs like misc and zero regs, which
        // can be recognized because the new mapping is the same as
        // the old one.
        // Also do not free previous reg if it was for a zero/one
        // idiom instruction.
        const bool using_hardwired_reg =
            (hb_it->prevPhysReg->classValue() == IntRegClass) &&
            freeList->isHardwiredReg(hb_it->prevPhysReg);

        if (((hb_it->newPhysReg != hb_it->prevPhysReg) || hb_it->isMove) &&
            !using_hardwired_reg)
        {
            freeList->addReg(hb_it->prevPhysReg);
        }

        ++stats.committedMaps;

        historyBuffer[tid].erase(hb_it--);
    }
}

template <class Impl>
inline void
DefaultRename<Impl>::renameSrcRegs(const DynInstPtr &inst, ThreadID tid)
{
    ThreadContext *tc = inst->tcBase();
    RenameMap *map = renameMap[tid];
    unsigned num_src_regs = inst->numSrcRegs();

    // Get the architectual register numbers from the source and
    // operands, and redirect them to the right physical register.
    for (int src_idx = 0; src_idx < num_src_regs; src_idx++) {
        const RegId& src_reg = inst->srcRegIdx(src_idx);
        PhysRegIdPtr renamed_reg;

        bool zero_pred_from_br = false;

        renamed_reg =
            map->lookup(tc->flattenRegId(src_reg), zero_pred_from_br);

        stats.numRenamedSrcBPred += zero_pred_from_br;

        switch (src_reg.classValue()) {
          case IntRegClass:
            stats.intLookups++;
            break;
          case FloatRegClass:
            stats.fpLookups++;
            break;
          case VecRegClass:
          case VecElemClass:
            stats.vecLookups++;
            break;
          case VecPredRegClass:
            stats.vecPredLookups++;
            break;
          case CCRegClass:
          case MiscRegClass:
            break;

          default:
            panic("Invalid register class: %d.", src_reg.classValue());
        }

        DPRINTF(Rename,
                "[tid:%i] "
                "Looking up %s arch reg %i, got phys reg %i (%s)\n",
                tid, src_reg.className(),
                src_reg.index(), renamed_reg->index(),
                renamed_reg->className());

        inst->renameSrcReg(src_idx, renamed_reg);

        // See if the register is ready or not.
        if (scoreboard->getReg(renamed_reg)) {
            DPRINTF(Rename,
                    "[tid:%i] "
                    "Register %d (flat: %d) (%s) is ready.\n",
                    tid, renamed_reg->index(), renamed_reg->flatIndex(),
                    renamed_reg->className());

            inst->markSrcRegReady(src_idx);
        } else {
            DPRINTF(Rename,
                    "[tid:%i] "
                    "Register %d (flat: %d) (%s) is not ready.\n",
                    tid, renamed_reg->index(), renamed_reg->flatIndex(),
                    renamed_reg->className());
        }

        ++stats.lookups;
    }
}

template <class Impl>
inline bool
DefaultRename<Impl>::renameDestRegs(const DynInstPtr &inst, ThreadID tid)
{
    ThreadContext *tc = inst->tcBase();
    RenameMap *map = renameMap[tid];
    int num_dest_regs = inst->numDestRegs();

    const bool zero_pred_br = enableBranchZeroPred &&
        ((inst->staticInst->isCbz() &&
          inst->readPredTaken()) ||
          (inst->staticInst->isCbnz() &&
          !inst->readPredTaken()));


    std::string dis = inst->staticInst->disassemble(
        inst->pcState().instAddr());

    const bool pri_pred_br = enableSubsConstPred &&
        inst->isControl() &&
        left != nullptr &&
        imm != -1 &&
        (imm >= -256 && imm <= 255) &&
        (((dis.find("b.eq") != std::string::npos) &&
        inst->readPredTaken()) ||
        ((dis.find("b.ne") != std::string::npos) &&
        !inst->readPredTaken()));

    if (zero_pred_br)
    {
       // Get cbz/cbnz arch src reg
       const RegId& src_reg = inst->srcRegIdx(0);
       RegId flat_src_regid = tc->flattenRegId(src_reg);

       // We'll make this a zero idiom
       std::bitset<16> idioms = 0x1;
       std::bitset<16> vps = 0x0;

       const bool is_move = false;
       PhysRegIdPtr move_reg = nullptr;
       RegId const * arch_move_reg = nullptr;
       const int width = (int) inst->staticInst->getIntWidth();

       auto rename_result = map->rename(flat_src_regid,
            idioms,
            vps,
            is_move,
            move_reg,
            width,
            arch_move_reg,
            0,
            zero_pred_br);

        assert(inst->seqNum != 0);

        // If br is mispred, this mapping has to go away, so seqnum-1
        RenameHistory hb_entry(inst->seqNum+1, flat_src_regid, is_move,
                               rename_result.first,
                               rename_result.second,
                               rename_result.prev_extended);

        historyBuffer[tid].push_front(hb_entry);
        stats.numBranchZeroPred++;
        DPRINTF(Rename, "Renamed src reg %d to zeroReg because cbz/cbnz\n",
                    inst->srcRegIdx(0).flatIndex());
    }
    else if (pri_pred_br)
    {
       //inst->printSmall();
       // Get subs arch src reg
       RegId flat_src_regid = tc->flattenRegId(left_arch);

       // We'll make this a zero idiom
       std::bitset<16> idioms = 0x0;
       idioms.set(15);

       std::bitset<16> vps = 0x0;

       const bool is_move = false;
       PhysRegIdPtr move_reg = nullptr;
       RegId const * arch_move_reg = nullptr;
       const int width = (int) inst->staticInst->getIntWidth();

       auto rename_result = map->rename(flat_src_regid,
            idioms,
            vps,
            is_move,
            move_reg,
            width,
            arch_move_reg,
            imm,
            pri_pred_br);

        assert(inst->seqNum != 0);

        // If br is mispred, this mapping has to go away, so seqnum-1
        RenameHistory hb_entry(inst->seqNum+1, flat_src_regid, is_move,
                               rename_result.first,
                               rename_result.second,
                               rename_result.prev_extended);

        historyBuffer[tid].push_front(hb_entry);
        stats.numShortImmBranchPred++;

        inst->setBranchConstPred();
    }

    // Rename the destination registers.
    for (int dest_idx = 0; dest_idx < num_dest_regs; dest_idx++) {
        const RegId& dest_reg = inst->destRegIdx(dest_idx);
        typename RenameMap::RenameInfo rename_result;

        RegId flat_dest_regid = tc->flattenRegId(dest_reg);
        flat_dest_regid.setNumPinnedWrites(dest_reg.getNumPinnedWrites());

        const bool zero_idiom = enableRenameIdioms &&
            (inst->staticInst->isZeroIdiom() || inst->isDynZeroIdiom());
        const bool one_idiom = enableRenameIdioms &&
            (inst->staticInst->isOneIdiom() || inst->isDynOneIdiom());

        const bool writes_to_zero_reg =
            flat_dest_regid.flatIndex() == TheISA::ZeroReg;

        const bool is_move = enableMoveElimination &&
            (inst->staticInst->isMove() || inst->isDynMove());

        stats.renamedZeroIdioms += zero_idiom;
        stats.renamedOneIdioms += one_idiom;

        std::bitset<16> idioms = (0x1 & zero_idiom) |
            ((0x1 & one_idiom) << 0x1);

        const int width = (int) inst->staticInst->getIntWidth();

        PhysRegIdPtr move_reg = nullptr;
        RegId const * arch_move_reg = nullptr;

        bool found = false;

        if (is_move && !inst->staticInst->isCsel() &&
            !inst->staticInst->isCsinc() && !inst->staticInst->isCsneg() &&
            !inst->staticInst->isDiv() && !inst->staticInst->isMul())
        {
            for (int i = 0; i < inst->numSrcRegs(); i++)
            {
                DPRINTF(Rename, "Looking at src reg %d (preg: %d)\n",
                    inst->srcRegIdx(i).flatIndex(),
                    inst->renamedSrcRegIdx(i)->flatIndex());

                if (inst->renamedSrcRegIdx(i) !=
                    freeList->hardwiredPhyReg(0x0))
                {
                    move_reg = inst->renamedSrcRegIdx(i);
                    arch_move_reg = &inst->srcRegIdx(i);
                    found = true;
                    break;
                }
            }
        }
        // mul/div case
        else if (is_move && (inst->staticInst->isDiv() ||
            inst->staticInst->isMul()))
        {
            for (int i = 0; i < inst->numSrcRegs(); i++)
            {
                DPRINTF(Rename, "Looking at src reg %d (preg: %d)\n",
                    inst->srcRegIdx(i).flatIndex(),
                    inst->renamedSrcRegIdx(i)->flatIndex());

                // Mul and div are moving if one of the
                // operand is 0x1 (mul) so move the other
                // or the second operand is 0x1 (div),
                // so also mov the other
                if (inst->renamedSrcRegIdx(i) !=
                    freeList->hardwiredPhyReg(0x1))
                {
                    move_reg = inst->renamedSrcRegIdx(i);
                    arch_move_reg = &inst->srcRegIdx(i);
                    found = true;
                    //std::cout << "Reusing reg idx " << i << "(";
                    //std::cout  << inst->renamedSrcRegIdx(i)->index();
                    //std::cout << ") for ";
                    //inst->printSmall();
                    break;
                }
            }
        }
        // Csel case
        else if (is_move)
        {
            found = true;
            move_reg = inst->renamedSrcRegIdx(inst->move_reg_idx);
            arch_move_reg = &inst->srcRegIdx(inst->move_reg_idx);
        }

        // Can happen if we get things like op r0, r0, r0 and we
        // previously renamed r0 to xzr
        if (is_move && !found)
        {
            move_reg = inst->renamedSrcRegIdx(0);
            arch_move_reg = &inst->srcRegIdx(0);
        }

        // In theory we won't predict move elim/idiom elim instructions
        const bool value_predicted =
            dest_idx > 1 ?
            false :
            inst->isValuePredictedIdx(dest_idx);

        const bool vp_zero = value_predicted &&
            enableRenameIdioms &&
            inst->getValuePrediction(dest_idx) == 0x0;
        const bool vp_one = value_predicted &&
            enableRenameIdioms &&
            inst->getValuePrediction(dest_idx) == 0x1;

        stats.renamedZeroVP += vp_zero;
        stats.renamedOneVP += vp_one;


        std::bitset<16> vps = (0x1 & vp_zero) |
            ((0x1 & vp_one) << 0x1);

        if (enableSmallConsts && value_predicted &&
            vps.none() &&
            is_9bit_signed(inst->getValuePrediction(dest_idx)))
        {
            vps.set(15);
        }

        const bool small_const = enableSmallConsts &&
            inst->staticInst->isSmallConst() &&
            !zero_idiom &&
            !one_idiom;

        uint64_t value = 0;

        if (small_const)
        {
            idioms.set(15);
            inst->fakeRenameDestRegs(freeList->hardwiredPhyReg(0x0));
            inst->execute();
            value = inst->getActualValue(dest_idx);
        }
        else if (value_predicted)
        {
            value = inst->getValuePrediction(dest_idx);
        }

        rename_result = map->rename(flat_dest_regid,
            idioms,
            vps,
            is_move,
            move_reg,
            width,
            arch_move_reg,
            value);

        if (!rename_result.success)
        {
            assert(inst->staticInst->isMove() ||
                inst->isDynMove());
            return false;
        }

        inst->flattenDestReg(dest_idx, flat_dest_regid);

        const bool move_eliminated = is_move && rename_result.did_move_elim;
        stats.renamedMoves += move_eliminated;
        stats.skippedMoveElim += is_move && !move_eliminated;

        // Record the rename information so that a history can be kept.
        RenameHistory hb_entry(inst->seqNum, flat_dest_regid, is_move,
                               rename_result.first,
                               rename_result.second,
                               rename_result.prev_extended);

        historyBuffer[tid].push_front(hb_entry);

        DPRINTF(Rename, "[tid:%i] [sn:%llu] "
                "Adding instruction to history buffer (size=%i).\n",
                tid,(*historyBuffer[tid].begin()).instSeqNum,
                historyBuffer[tid].size());

        // Tell the instruction to rename the appropriate destination
        // register (dest_idx) to the new physical register
        // (rename_result.first), and record the previous physical
        // register that the same logical register was renamed to
        // (rename_result.second).
        inst->renameDestReg(dest_idx,
                            rename_result.first,
                            rename_result.second);

        if (small_const)
        {
            // True so it does not count in reg read/write
            scoreboard->setReg(rename_result.first, true);
        }

        if (move_eliminated)
        {
            inst->setMoveEliminated();
        }

        //assert(!value_predicted &&
        //    (zero_idiom || one_idiom ||
        //     move_eliminated || writes_to_zero_reg));

        if (idioms.none() &&
            !writes_to_zero_reg &&
            !move_eliminated &&
            !small_const)
        {
            // Only value predict the first dest reg for now
            if (!value_predicted)
            {
                scoreboard->unsetReg(rename_result.first);
            }
            else
            {
                if (vps.none() || vps.test(15))
                {
                    const bool vp_ignore_preg =
                        vps.test(15);
                    scoreboard->setReg(rename_result.first,
                      vp_ignore_preg);
                    inst->setVPIntRegOperand(
                      inst->staticInst.get(),
                      dest_idx,
                      inst->getValuePrediction(dest_idx));
                }
            }
        }

        ++stats.renamedOperands;
    }

    return true;
}

template <class Impl>
inline int
DefaultRename<Impl>::calcFreeROBEntries(ThreadID tid)
{
    int num_free = freeEntries[tid].robEntries -
                  (instsInProgress[tid] - fromIEW->iewInfo[tid].dispatched);

    //DPRINTF(Rename,"[tid:%i] %i rob free\n",tid,num_free);

    return num_free;
}

template <class Impl>
inline int
DefaultRename<Impl>::calcFreeIQEntries(ThreadID tid)
{
    int num_free = freeEntries[tid].iqEntries -
                  (instsInProgress[tid] - fromIEW->iewInfo[tid].dispatched);

    //DPRINTF(Rename,"[tid:%i] %i iq free\n",tid,num_free);

    return num_free;
}

template <class Impl>
inline int
DefaultRename<Impl>::calcFreeLQEntries(ThreadID tid)
{
        int num_free = freeEntries[tid].lqEntries -
                                  (loadsInProgress[tid] - fromIEW->iewInfo[tid].dispatchedToLQ);
        DPRINTF(Rename,
                "calcFreeLQEntries: free lqEntries: %d, loadsInProgress: %d, "
                "loads dispatchedToLQ: %d\n",
                freeEntries[tid].lqEntries, loadsInProgress[tid],
                fromIEW->iewInfo[tid].dispatchedToLQ);
        return num_free;
}

template <class Impl>
inline int
DefaultRename<Impl>::calcFreeSQEntries(ThreadID tid)
{
        int num_free = freeEntries[tid].sqEntries -
                                  (storesInProgress[tid] - fromIEW->iewInfo[tid].dispatchedToSQ);
        DPRINTF(Rename, "calcFreeSQEntries: free sqEntries: %d, storesInProgress: %d, "
                "stores dispatchedToSQ: %d\n", freeEntries[tid].sqEntries,
                storesInProgress[tid], fromIEW->iewInfo[tid].dispatchedToSQ);
        return num_free;
}

template <class Impl>
unsigned
DefaultRename<Impl>::validInsts()
{
    unsigned inst_count = 0;

    for (int i=0; i<fromDecode->size; i++) {
        if (!fromDecode->insts[i]->isSquashed())
            inst_count++;
    }

    return inst_count;
}

template <class Impl>
void
DefaultRename<Impl>::readStallSignals(ThreadID tid)
{
    if (fromIEW->iewBlock[tid]) {
        stalls[tid].iew = true;
    }

    if (fromIEW->iewUnblock[tid]) {
        assert(stalls[tid].iew);
        stalls[tid].iew = false;
    }
}

template <class Impl>
bool
DefaultRename<Impl>::checkStall(ThreadID tid)
{
    bool ret_val = false;

    if (stalls[tid].iew) {
        DPRINTF(Rename,"[tid:%i] Stall from IEW stage detected.\n", tid);
        ret_val = true;
    } else if (calcFreeROBEntries(tid) <= 0) {
        DPRINTF(Rename,"[tid:%i] Stall: ROB has 0 free entries.\n", tid);
        ret_val = true;
    } else if (calcFreeIQEntries(tid) <= 0) {
        DPRINTF(Rename,"[tid:%i] Stall: IQ has 0 free entries.\n", tid);
        ret_val = true;
    } else if (calcFreeLQEntries(tid) <= 0 && calcFreeSQEntries(tid) <= 0) {
        DPRINTF(Rename,"[tid:%i] Stall: LSQ has 0 free entries.\n", tid);
        ret_val = true;
    } else if (renameMap[tid]->numFreeEntries() <= 0) {
        DPRINTF(Rename,"[tid:%i] Stall: RenameMap has 0 free entries.\n", tid);
        ret_val = true;
    } else if (renameStatus[tid] == SerializeStall &&
               (!emptyROB[tid] || instsInProgress[tid])) {
        DPRINTF(Rename,"[tid:%i] Stall: Serialize stall and ROB is not "
                "empty.\n",
                tid);
        ret_val = true;
    }

    return ret_val;
}

template <class Impl>
void
DefaultRename<Impl>::readFreeEntries(ThreadID tid)
{
    if (fromIEW->iewInfo[tid].usedIQ)
        freeEntries[tid].iqEntries = fromIEW->iewInfo[tid].freeIQEntries;

    if (fromIEW->iewInfo[tid].usedLSQ) {
        freeEntries[tid].lqEntries = fromIEW->iewInfo[tid].freeLQEntries;
        freeEntries[tid].sqEntries = fromIEW->iewInfo[tid].freeSQEntries;
    }

    if (fromCommit->commitInfo[tid].usedROB) {
        freeEntries[tid].robEntries =
            fromCommit->commitInfo[tid].freeROBEntries;
        emptyROB[tid] = fromCommit->commitInfo[tid].emptyROB;
    }

    DPRINTF(Rename, "[tid:%i] Free IQ: %i, Free ROB: %i, "
                    "Free LQ: %i, Free SQ: %i, FreeRM %i(%i %i %i %i %i)\n",
            tid,
            freeEntries[tid].iqEntries,
            freeEntries[tid].robEntries,
            freeEntries[tid].lqEntries,
            freeEntries[tid].sqEntries,
            renameMap[tid]->numFreeEntries(),
            renameMap[tid]->numFreeIntEntries(),
            renameMap[tid]->numFreeFloatEntries(),
            renameMap[tid]->numFreeVecEntries(),
            renameMap[tid]->numFreePredEntries(),
            renameMap[tid]->numFreeCCEntries());

    DPRINTF(Rename, "[tid:%i] %i instructions not yet in ROB\n",
            tid, instsInProgress[tid]);
}

template <class Impl>
bool
DefaultRename<Impl>::checkSignalsAndUpdate(ThreadID tid)
{
    // Check if there's a squash signal, squash if there is
    // Check stall signals, block if necessary.
    // If status was blocked
    //     check if stall conditions have passed
    //         if so then go to unblocking
    // If status was Squashing
    //     check if squashing is not high.  Switch to running this cycle.
    // If status was serialize stall
    //     check if ROB is empty and no insts are in flight to the ROB

    readFreeEntries(tid);
    readStallSignals(tid);

    if (fromCommit->commitInfo[tid].squash) {
        DPRINTF(Rename, "[tid:%i] Squashing instructions due to squash from "
                "commit.\n", tid);

        nzcv_available =
          (!fromCommit->commitInfo[tid].mispredictInst &&
          nzcv_available &&
          (fromCommit->commitInfo[tid].doneSeqNum >= nzcv_seq_num)) ||
          (fromCommit->commitInfo[tid].mispredictInst &&
          fromCommit->commitInfo[tid].mispredictInst->isCondCtrl() &&
          !fromCommit->commitInfo[tid].mispredictInst->staticInst->isCbz() &&
          !fromCommit->commitInfo[tid].mispredictInst->staticInst->isCbnz() &&
          !fromCommit->commitInfo[tid].mispredictInst->staticInst->isTbz());

        left = nullptr;
        right = nullptr;
        imm = -1;

        squash(fromCommit->commitInfo[tid].doneSeqNum, tid);

        return true;
    }

    if (checkStall(tid)) {
        return block(tid);
    }

    if (renameStatus[tid] == Blocked) {
        DPRINTF(Rename, "[tid:%i] Done blocking, switching to unblocking.\n",
                tid);

        renameStatus[tid] = Unblocking;

        unblock(tid);

        return true;
    }

    if (renameStatus[tid] == Squashing) {
        // Switch status to running if rename isn't being told to block or
        // squash this cycle.
        if (resumeSerialize) {
            DPRINTF(Rename,
                    "[tid:%i] Done squashing, switching to serialize.\n", tid);

            renameStatus[tid] = SerializeStall;
            return true;
        } else if (resumeUnblocking) {
            DPRINTF(Rename,
                    "[tid:%i] Done squashing, switching to unblocking.\n",
                    tid);
            renameStatus[tid] = Unblocking;
            return true;
        } else {
            DPRINTF(Rename, "[tid:%i] Done squashing, switching to running.\n",
                    tid);
            renameStatus[tid] = Running;
            return false;
        }
    }

    if (renameStatus[tid] == SerializeStall) {
        // Stall ends once the ROB is free.
        DPRINTF(Rename, "[tid:%i] Done with serialize stall, switching to "
                "unblocking.\n", tid);

        DynInstPtr serial_inst = serializeInst[tid];

        renameStatus[tid] = Unblocking;

        unblock(tid);

        DPRINTF(Rename, "[tid:%i] Processing instruction [%lli] with "
                "PC %s.\n", tid, serial_inst->seqNum, serial_inst->pcState());

        // Put instruction into queue here.
        serial_inst->clearSerializeBefore();

        if (!skidBuffer[tid].empty()) {
            skidBuffer[tid].push_front(serial_inst);
        } else {
            insts[tid].push_front(serial_inst);
        }

        DPRINTF(Rename, "[tid:%i] Instruction must be processed by rename."
                " Adding to front of list.\n", tid);

        serializeInst[tid] = NULL;

        return true;
    }

    // If we've reached this point, we have not gotten any signals that
    // cause rename to change its status.  Rename remains the same as before.
    return false;
}

template<class Impl>
void
DefaultRename<Impl>::serializeAfter(InstQueue &inst_list, ThreadID tid)
{
    if (inst_list.empty()) {
        // Mark a bit to say that I must serialize on the next instruction.
        serializeOnNextInst[tid] = true;
        return;
    }

    // Set the next instruction as serializing.
    inst_list.front()->setSerializeBefore();
}

template <class Impl>
inline bool
DefaultRename<Impl>::canStrReduceToZeroIdiom(const DynInstPtr &inst)
{
    if (inst->staticInst->isMove() |
        inst->staticInst->isZeroIdiom() |
        inst->staticInst->isCC())
    {
        return false;
    }

    const bool sub_to_zero_idiom = inst->staticInst->isSub() &&
        inst->numSrcRegs() == 1 &&
        inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1) &&
        inst->staticInst->immIsOne();

    if (sub_to_zero_idiom)
    {
        return true;
    }

    const bool shl_to_zero_idiom = inst->staticInst->isShl() &&
        ((inst->numSrcRegs() == 1 &&
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0))) ||
        (inst->numSrcRegs() == 2 &&
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0))));

    const bool shr_to_zero_idiom = inst->staticInst->isShr() &&
        ((inst->numSrcRegs() == 1 &&
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0))) ||
        (inst->numSrcRegs() == 2 &&
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0))));


    if (shl_to_zero_idiom | shr_to_zero_idiom)
    {
        return true;
    }

    const bool ubfm_to_zero_idiom = inst->staticInst->isUbfm() &&
         inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0);

    if (ubfm_to_zero_idiom)
    {
        return true;
    }

    const bool bic_to_zero_idiom = inst->staticInst->isBic() &&
          (inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0));

    if (bic_to_zero_idiom)
    {
        return true;
    }

    const bool rbit_to_zero_idiom = inst->staticInst->isRbit() &&
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0));

    if (rbit_to_zero_idiom)
    {
        return true;
    }

    const bool pot_csinc_to_zero_idiom = inst->staticInst->isCsinc() &&
        nzcv_available;

    if (pot_csinc_to_zero_idiom)
    {
        inst->fakeRenameDestRegs(freeList->hardwiredPhyReg(0x0));
        inst->execute();
        inst->clearValueMispredicted();
        // Adjust CPU intReg count : we did not really read the int reg...
        // Also did not read the cc regs but we won't be looking at those
        // Beware, if src is hardwired, it will already be ignored.
        const bool ignored_read_1 =
            inst->renamedSrcRegIdx(3)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(3)) &&
            inst->renamedSrcRegIdx(3)->index() > 1;
        const bool ignored_read_2 =
              inst->renamedSrcRegIdx(4)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(4)) &&
            inst->renamedSrcRegIdx(4)->index() > 1;
        cpu->cpuStats.intRegfileReads -=
            ignored_read_1 + ignored_read_2;

        return inst->staticInst->scratch &&
            (inst->renamedSrcRegIdx(3) == freeList->hardwiredPhyReg(0x0));
    }

    const bool pot_csel_to_zero_idiom = inst->staticInst->isCsel() &&
        nzcv_available;

    if (pot_csel_to_zero_idiom)
    {
        inst->fakeRenameDestRegs(freeList->hardwiredPhyReg(0x0));
        inst->execute();
        inst->clearValueMispredicted();
        // Adjust CPU intReg count : we did not really read the int reg...
        // Also did not read the cc regs but we won't be looking at those
        // Beware, if src is hardwired, it will already be ignored.
       const bool ignored_read_1 =
            inst->renamedSrcRegIdx(3)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(3)) &&
            inst->renamedSrcRegIdx(3)->index() > 1;
        const bool ignored_read_2 =
              inst->renamedSrcRegIdx(4)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(4)) &&
            inst->renamedSrcRegIdx(4)->index() > 1;
        cpu->cpuStats.intRegfileReads -=
            ignored_read_1 + ignored_read_2;

        return (inst->staticInst->scratch &&
            (inst->renamedSrcRegIdx(3) == freeList->hardwiredPhyReg(0x0))) ||
            (!inst->staticInst->scratch &&
            (inst->renamedSrcRegIdx(4) == freeList->hardwiredPhyReg(0x0)));
    }

    const bool mul_to_zero_idiom = inst->staticInst->isMul() &&
         (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0) ||
          inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0));

    const bool div_to_zero_idiom = inst->staticInst->isDiv() &&
        inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0);

    return mul_to_zero_idiom |
        div_to_zero_idiom;
}

template <class Impl>
inline bool
DefaultRename<Impl>::canStrReduceToOneIdiom(const DynInstPtr &inst)
{
    if (inst->staticInst->isMove() |
        !inst->staticInst->immIsOne() |
        (inst->numSrcRegs() != 1 &&
            !inst->staticInst->isCsinc()) |
        inst->staticInst->isCC())
    {
        return false;
    }

    const bool add_to_one_idiom = inst->staticInst->isAdd() &&
        inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0);

    const bool orr_to_one_idiom = inst->staticInst->isOr() &&
        inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0);

    const bool xor_to_one_idiom = inst->staticInst->isXor() &&
        inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0);

    const bool and_to_one_idiom = inst->staticInst->isAnd() &&
        inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1);

    if (add_to_one_idiom | orr_to_one_idiom |
        xor_to_one_idiom | and_to_one_idiom)
    {
        return true;
    }

    const bool pot_csel_to_one_idiom = inst->staticInst->isCsel() &&
        nzcv_available;

    if (pot_csel_to_one_idiom)
    {
        inst->fakeRenameDestRegs(freeList->hardwiredPhyReg(0x0));
        inst->execute();
        inst->clearValueMispredicted();
        // Adjust CPU intReg count : we did not really read the int reg...
        // Also did not read the cc regs but we won't be looking at those
        // Beware, if src is hardwired, it will already be ignored.
       const bool ignored_read_1 =
            inst->renamedSrcRegIdx(3)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(3)) &&
            inst->renamedSrcRegIdx(3)->index() > 1;
        const bool ignored_read_2 =
              inst->renamedSrcRegIdx(4)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(4)) &&
            inst->renamedSrcRegIdx(4)->index() > 1;
        cpu->cpuStats.intRegfileReads -=
            ignored_read_1 + ignored_read_2;

        return (inst->staticInst->scratch &&
            (inst->renamedSrcRegIdx(3) == freeList->hardwiredPhyReg(0x1))) ||
            (!inst->staticInst->scratch &&
            inst->renamedSrcRegIdx(4) == freeList->hardwiredPhyReg(0x1));
    }

    const bool pot_csinc_to_one_idiom = inst->staticInst->isCsinc() &&
        nzcv_available;

    if (pot_csinc_to_one_idiom)
    {
        inst->fakeRenameDestRegs(freeList->hardwiredPhyReg(0x0));
        inst->execute();
        inst->clearValueMispredicted();
        // Adjust CPU intReg count : we did not really read the int reg...
        // Also did not read the cc regs but we won't be looking at those
        // Beware, if src is hardwired, it will already be ignored.
       const bool ignored_read_1 =
            inst->renamedSrcRegIdx(3)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(3)) &&
            inst->renamedSrcRegIdx(3)->index() > 1;
        const bool ignored_read_2 =
              inst->renamedSrcRegIdx(4)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(4)) &&
            inst->renamedSrcRegIdx(4)->index() > 1;
        cpu->cpuStats.intRegfileReads -=
            ignored_read_1 + ignored_read_2;

        return (inst->staticInst->scratch &&
            (inst->renamedSrcRegIdx(3) == freeList->hardwiredPhyReg(0x1))) ||
            (!inst->staticInst->scratch &&
            inst->renamedSrcRegIdx(4) == freeList->hardwiredPhyReg(0x0));
    }

    return false;
}

template <class Impl>
inline bool
DefaultRename<Impl>::canStrReduceToMove(const DynInstPtr &inst)
{
    if (inst->staticInst->isMove() |
        inst->staticInst->isZeroIdiom() |
        (!inst->staticInst->isCsel() &
        !inst->staticInst->isCsinc() &
        !inst->staticInst->isCsneg() &
        !inst->staticInst->isBic() &
        (inst->numSrcRegs() != 2)) |
        inst->staticInst->isCC())
    {
        return false;
    }

    // Register index is actually from right to left...
    const bool add_to_move = inst->staticInst->isAdd() &
        (((inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0)) &
            inst->staticInst->immIsZero() & !inst->staticInst->isExtend()) |
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0)));

    if (add_to_move)
    {
        return true;
    }

    const bool sub_to_move = inst->staticInst->isSub() &
        !inst->staticInst->isExtend() &
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0));

    if (sub_to_move)
    {
        return true;
    }

    const bool orr_to_move = inst->staticInst->isOr() &
        !inst->staticInst->isExtend() &
        (((inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0)) &
            inst->staticInst->immIsZero()) |
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0)));

    if (orr_to_move)
    {
        return true;
    }

    const bool xor_to_move = inst->staticInst->isXor() &&
        !inst->staticInst->isExtend() &
        (((inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0)) &&
            inst->staticInst->immIsZero()) ||
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0)));

    if (xor_to_move)
    {
        return true;
    }

    const bool shl_to_move = inst->staticInst->isShl() &
        !inst->staticInst->isExtend() &
        (inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0));

    const bool shr_to_move = inst->staticInst->isShr() &
        !inst->staticInst->isExtend() &
        (inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0));

    if (shl_to_move | shr_to_move)
    {
        return true;
    }

    // If csel can determine the condition now, then it can
    // become a move instruction, but we need to find the
    // correct source (later in renameDestRegs)
    const bool csel_to_move = inst->staticInst->isCsel() &
        nzcv_available;

    if (csel_to_move)
    {
        // We are trying to find out if condition is correct or not
        inst->fakeRenameDestRegs(freeList->hardwiredPhyReg(0x0));
        inst->execute();
        inst->clearValueMispredicted();
        // Adjust CPU intReg count : we did not really read the int reg...
        // Also did not read the cc regs but we won't be looking at those
        // Beware, if src is hardwired, it will already be ignored.
       const bool ignored_read_1 =
            inst->renamedSrcRegIdx(3)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(3)) &&
            inst->renamedSrcRegIdx(3)->index() > 1;
        const bool ignored_read_2 =
              inst->renamedSrcRegIdx(4)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(4)) &&
            inst->renamedSrcRegIdx(4)->index() > 1;
        cpu->cpuStats.intRegfileReads -=
            ignored_read_1 + ignored_read_2;

        inst->move_reg_idx = inst->staticInst->scratch ? 3 : 4;
        return (inst->renamedSrcRegIdx(inst->move_reg_idx) !=
            freeList->hardwiredPhyReg(0x0)) &&
            (inst->renamedSrcRegIdx(inst->move_reg_idx) !=
            freeList->hardwiredPhyReg(0x1));
    }

    // If csinc can determine the condition now, then it can
    // become a move instruction, but we need to find the
    // correct source (later in renameDestRegs)
    const bool csinc_to_move = inst->staticInst->isCsinc() &
        nzcv_available;

    if (csinc_to_move)
    {
        // We are trying to find out if condition is correct or not
        inst->fakeRenameDestRegs(freeList->hardwiredPhyReg(0x0));
        inst->execute();
        inst->clearValueMispredicted();
        // Adjust CPU intReg count : we did not really read the int reg...
        // Also did not read the cc regs but we won't be looking at those
        // Beware, if src is hardwired, it will already be ignored.
       const bool ignored_read_1 =
            inst->renamedSrcRegIdx(3)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(3)) &&
            inst->renamedSrcRegIdx(3)->index() > 1;
        const bool ignored_read_2 =
              inst->renamedSrcRegIdx(4)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(4)) &&
            inst->renamedSrcRegIdx(4)->index() > 1;
        cpu->cpuStats.intRegfileReads -=
            ignored_read_1 + ignored_read_2;

        inst->move_reg_idx = inst->staticInst->scratch ? 3 : 4;
        return inst->staticInst->scratch &&
            (inst->renamedSrcRegIdx(inst->move_reg_idx) !=
            freeList->hardwiredPhyReg(0x0)) &&
            (inst->renamedSrcRegIdx(inst->move_reg_idx) !=
            freeList->hardwiredPhyReg(0x1));
    }

    // If csneg can determine the condition now, then it can
    // become a move instruction, but we need to find the
    // correct source (later in renameDestRegs)
    const bool csneg_to_move = inst->staticInst->isCsneg() &
        nzcv_available;

    if (csneg_to_move)
    {
        // We are trying to find out if condition is correct or not
        inst->fakeRenameDestRegs(freeList->hardwiredPhyReg(0x0));
        inst->execute();
        inst->clearValueMispredicted();
        // Adjust CPU intReg count : we did not really read the int reg...
        // Also did not read the cc regs but we won't be looking at those
        // Beware, if src is hardwired, it will already be ignored.
       const bool ignored_read_1 =
            inst->renamedSrcRegIdx(3)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(3)) &&
            inst->renamedSrcRegIdx(3)->index() > 1;
        const bool ignored_read_2 =
              inst->renamedSrcRegIdx(4)->isIntPhysReg() &&
            !scoreboard->getRegVP(inst->renamedSrcRegIdx(4)) &&
            inst->renamedSrcRegIdx(4)->index() > 1;
        cpu->cpuStats.intRegfileReads -=
            ignored_read_1 + ignored_read_2;

        inst->move_reg_idx = inst->staticInst->scratch ? 3 : 4;
        return inst->staticInst->scratch;
    }

    const bool bic_to_move = inst->staticInst->isBic() &&
          !inst->staticInst->isExtend() &
          (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0));

    if (bic_to_move)
    {
        return true;
    }

    const bool mul_to_move = inst->staticInst->isMul() &
        !inst->staticInst->isExtend() &
        ((inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x1)) |
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1)));

    // For some reason UDiv and SDiv don't have the
    // operands in the same order...
    const bool div_to_move = ((inst->staticInst->isUDiv() &
        !inst->staticInst->isExtend() &
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1))))
        ||
        ((inst->staticInst->isSDiv() &
        !inst->staticInst->isExtend() &
        (inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x1))));

    return mul_to_move |
        div_to_move;
}

template <class Impl>
inline bool
DefaultRename<Impl>::canStrReduceToNop(const DynInstPtr &inst)
{
    if (!inst->staticInst->isCondCtrl() &
        !inst->staticInst->isCC())
    {
        return false;
    }

    const bool cbz_to_nop = (inst->staticInst->isCbz() |
        inst->staticInst->isCbnz() |
        inst->staticInst->isTbz()) &
        ((inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0)) |
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1)));

    if (cbz_to_nop)
    {
        return true;
    }

    // Arguably not doable without a full ALU
    // TODO : Gate with parameter
    const bool ands_to_nop_with_alu = enableEarlyALU
        && inst->staticInst->isAnd() &
        (inst->numSrcRegs() == 1)
        &
          ((inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0))
          |
          (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1)));

    // ands 0x0, ? or ands ?, 0x0, essentially
    const bool ands_to_nop_without_alu = inst->staticInst->isAnd() &
        (inst->numSrcRegs() == 2)
        &
          ((inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0))
          |
          (inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0))
          |
          ((inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1)) &
            (inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x1))))
          ;

    if (ands_to_nop_with_alu | ands_to_nop_without_alu)
    {
        return true;
    }

    // Arguably not doable without a full ALU
    // TODO : Gate with parameter
    const bool subs_to_nop_with_alu = enableEarlyALU &&
        inst->staticInst->isSub() &
        (inst->numSrcRegs() == 1) &
        ((inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0)) |
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1)));

    // subs 0, 1 or subs 1, 0, essentially
    const bool subs_to_nop_without_alu = inst->staticInst->isSub() &
        (inst->numSrcRegs() == 2)
        &
          ((inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0))
          |
          (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1)))
        &
          ((inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0))
          |
          (inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x1)));


    if (subs_to_nop_with_alu | subs_to_nop_without_alu)
    {
        return true;
    }

    // Arguably not doable without a full ALU
    // TODO : Gate with parameter
    const bool adds_to_nop_with_alu = enableEarlyALU &&
        inst->staticInst->isAdd() &
        (inst->numSrcRegs() == 1) &
        ((inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0)) |
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1)));

    // adds 0, 1 or adds 1, 0, essentially
    const bool adds_to_nop_without_alu = inst->staticInst->isAdd() &
        (inst->numSrcRegs() == 2)
        &
          ((inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0))
          |
          (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1)))
        &
          ((inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0))
          |
          (inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x1)));

    if (adds_to_nop_with_alu | adds_to_nop_without_alu)
    {
        return true;
    }

    // Ca m'a l'air faux en fait...
    /*const bool ccmp_to_nop_with_alu =  enableEarlyALU &&
        inst->staticInst->isCcmp() &
        (inst->numSrcRegs() == 1) &
        ((inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0)) |
        (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1)));

    const bool ccmp_to_nop_without_alu = inst->staticInst->isCcmp() &
        (inst->numSrcRegs() == 2)
        &
          ((inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x0))
          |
          (inst->renamedSrcRegIdx(0) == freeList->hardwiredPhyReg(0x1)))
        &
          ((inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x0))
          |
          (inst->renamedSrcRegIdx(1) == freeList->hardwiredPhyReg(0x1)));

    if (ccmp_to_nop_with_alu | ccmp_to_nop_without_alu)
    {
        return true;
    }*/

    const bool branch_to_nop = inst->staticInst->isCondCtrl() &
        !inst->staticInst->isTbz() & !inst->staticInst->isCbz() &
        !inst->staticInst->isCbnz() &
        nzcv_available;

    return branch_to_nop;
}

template <class Impl>
inline void
DefaultRename<Impl>::incrFullStat(const FullSource &source)
{
    switch (source) {
      case ROB:
        ++stats.ROBFullEvents;
        break;
      case IQ:
        ++stats.IQFullEvents;
        break;
      case LQ:
        ++stats.LQFullEvents;
        break;
      case SQ:
        ++stats.SQFullEvents;
        break;
      default:
        panic("Rename full stall stat should be incremented for a reason!");
        break;
    }
}

template <class Impl>
void
DefaultRename<Impl>::dumpHistory()
{
    typename std::list<RenameHistory>::iterator buf_it;

    for (ThreadID tid = 0; tid < numThreads; tid++) {

        buf_it = historyBuffer[tid].begin();

        while (buf_it != historyBuffer[tid].end()) {
            cprintf("Seq num: %i\nArch reg[%s]: %i New phys reg:"
                    " %i[%s] Old phys reg: %i[%s]\n",
                    (*buf_it).instSeqNum,
                    (*buf_it).archReg.className(),
                    (*buf_it).archReg.index(),
                    (*buf_it).newPhysReg->index(),
                    (*buf_it).newPhysReg->className(),
                    (*buf_it).prevPhysReg->index(),
                    (*buf_it).prevPhysReg->className());

            buf_it++;
        }
    }
}

#endif//__CPU_O3_RENAME_IMPL_HH__
