# Copyright (c) 2016, 2019 ARM Limited
# All rights reserved.
#
# The license below extends only to copyright in the software and shall
# not be construed as granting a license to any other intellectual
# property including but not limited to intellectual property relating
# to a hardware implementation of the functionality of the software
# licensed hereunder.  You may use the software subject to the license
# terms below provided that you ensure that this notice is replicated
# unmodified and in its entirety in all distributions of the software,
# modified or unmodified, in source code or in binary form.
#
# Copyright (c) 2005-2007 The Regents of The University of Michigan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function

from m5.defines import buildEnv
from m5.params import *
from m5.proxy import *

from m5.objects.BaseCPU import BaseCPU
from m5.objects.FUPool import *
from m5.objects.O3Checker import O3Checker
from m5.objects.BranchPredictor import *
from m5.objects.ValuePredictor import *

class SMTFetchPolicy(ScopedEnum):
    vals = [ 'RoundRobin', 'Branch', 'IQCount', 'LSQCount' ]

class SMTQueuePolicy(ScopedEnum):
    vals = [ 'Dynamic', 'Partitioned', 'Threshold' ]

class CommitPolicy(ScopedEnum):
    vals = [ 'Aggressive', 'RoundRobin', 'OldestReady' ]

class DerivO3CPU(BaseCPU):
    type = 'DerivO3CPU'
    cxx_header = 'cpu/o3/deriv.hh'

    @classmethod
    def memory_mode(cls):
        return 'timing'

    @classmethod
    def require_caches(cls):
        return True

    @classmethod
    def support_take_over(cls):
        return True

    activity = Param.Unsigned(0, "Initial count")

    cacheStorePorts = Param.Unsigned(200, "Cache Ports. "
          "Constrains stores only.")
    cacheLoadPorts = Param.Unsigned(200, "Cache Ports. "
          "Constrains loads only.")

    decodeToFetchDelay = Param.Cycles(1, "Decode to fetch delay")
    renameToFetchDelay = Param.Cycles(1 ,"Rename to fetch delay")
    iewToFetchDelay = Param.Cycles(1, "Issue/Execute/Writeback to fetch "
                                   "delay")
    commitToFetchDelay = Param.Cycles(1, "Commit to fetch delay")
    fetchWidth = Param.Unsigned(16, "Fetch width")
    fetchBufferSize = Param.Unsigned(64, "Fetch buffer size in bytes")
    fetchQueueSize = Param.Unsigned(32, "Fetch queue size in micro-ops "
                                    "per-thread")

    renameToDecodeDelay = Param.Cycles(1, "Rename to decode delay")
    iewToDecodeDelay = Param.Cycles(1, "Issue/Execute/Writeback to decode "
                                    "delay")
    commitToDecodeDelay = Param.Cycles(1, "Commit to decode delay")
    fetchToDecodeDelay = Param.Cycles(3, "Fetch to decode delay")
    decodeWidth = Param.Unsigned(8, "Decode width")

    fetchLowConfThreshold = Param.Unsigned(1024,
        "Stop fetching if more than X lowconf branches in flight")

    enableValuePrediction = Param.Unsigned(0, "Enable VP")
    delayAfterVPMispred = Param.Unsigned(250, "Number of cycles "
        "where value predictions are not used after a VP mispred")
    enableRenameIdioms = Param.Unsigned(0, "Enable rename idioms")
    enableExtendedRenameIdioms = Param.Unsigned(0,
        "Enable 8/16 rename idioms")
    enableSmallConsts = Param.Unsigned(0, "Enable small consts")
    enableEarlyBranchResolution = Param.Unsigned(0,
        "Enable early br resolution when possible")
    enableBranchZeroPred = Param.Unsigned(0, "Enable zero pred through BP")
    enableSubsConstPred = Param.Unsigned(0, "Enable const pred through BP")
    enableMoveElimination = Param.Unsigned(0, "Enable move elimination")
    enableDynStrReduction = Param.Unsigned(0, "Enable DSR")
    enableEarlyALU = Param.Unsigned(0,
        "Enable Rename ALU for ands/subs/adds/ccmp")
    enableCountPotentialVPBP = Param.Unsigned(1, "Enable to count potential "
        " BPVP readers")
    iewToRenameDelay = Param.Cycles(1, "Issue/Execute/Writeback to rename "
                                    "delay")
    commitToRenameDelay = Param.Cycles(1, "Commit to rename delay")
    decodeToRenameDelay = Param.Cycles(1, "Decode to rename delay")
    renameWidth = Param.Unsigned(8, "Rename width")

    commitToIEWDelay = Param.Cycles(1, "Commit to "
               "Issue/Execute/Writeback delay")
    renameToIEWDelay = Param.Cycles(3, "Rename to "
               "Issue/Execute/Writeback delay")
    issueToExecuteDelay = Param.Cycles(1, "Issue to execute delay (internal "
              "to the IEW stage)")
    dispatchWidth = Param.Unsigned(8, "Dispatch width")
    issueWidth = Param.Unsigned(15, "Issue width")
    wbWidth = Param.Unsigned(15, "Writeback width")
    fuPool = Param.FUPool(DefaultFUPool(), "Functional Unit pool")

    iewToCommitDelay = Param.Cycles(1, "Issue/Execute/Writeback to commit "
               "delay")
    renameToROBDelay = Param.Cycles(1, "Rename to reorder buffer delay")
    commitWidth = Param.Unsigned(8, "Commit width")
    squashWidth = Param.Unsigned(1024, "Squash width")
    trapLatency = Param.Cycles(13, "Trap latency")
    fetchTrapLatency = Param.Cycles(1, "Fetch trap latency")

    valueMispredAtIEW = Param.Unsigned(1, "Validate VP at IEW")
    valueMispredAtCommit = Param.Unsigned(0, "Validate VP a Commit")

    backComSize = Param.Unsigned(5,
        "Time buffer size for backwards communication")
    forwardComSize = Param.Unsigned(8,
        "Time buffer size for forward communication")

    LQEntries = Param.Unsigned(74, "Number of load queue entries")
    SQEntries = Param.Unsigned(53, "Number of store queue entries")
    LSQDepCheckShift = Param.Unsigned(0,
        "Number of places to shift addr before check")
    LSQCheckLoads = Param.Bool(True,
        "Should dependency violations be checked for loads & stores or just stores")
    store_set_clear_period = Param.Unsigned(500000,
            "Number of load/store insts before the predictor be invalidated")
    LFSTSize = Param.Unsigned(2048, "Last fetched store table size")
    SSITSize = Param.Unsigned(2048, "Store set ID table size")

    numRobs = Param.Unsigned(1, "Number of Reorder Buffers")

    # Remove 1 reg (0 + 1-idiom) or 6 regs (0 + 1/2/4/8/16/32) idioms
    # 315 + 32 + 24
    numPhysIntRegs = Param.Unsigned(292 + 768,
        "Number of physical integer registers")
    numPhysFloatRegs = Param.Unsigned(292,
        "Number of physical floating point registers")

    # most ISAs don't use condition-code regs, so default is 0
    _defaultNumPhysCCRegs = 0
    if buildEnv['TARGET_ISA'] in ('arm','x86'):
        # For x86, each CC reg is used to hold only a subset of the
        # flags, so we need 4-5 times the number of CC regs as
        # physical integer regs to be sure we don't run out.  In
        # typical real machines, CC regs are not explicitly renamed
        # (it's a side effect of int reg renaming), so they should
        # never be the bottleneck here.
        _defaultNumPhysCCRegs = Self.numPhysIntRegs
    numPhysVecRegs = Param.Unsigned(292, "Number of physical vector "
                                      "registers")
    numPhysVecPredRegs = Param.Unsigned(32, "Number of physical predicate "
                                      "registers")
    numPhysCCRegs = Param.Unsigned(_defaultNumPhysCCRegs,
                                   "Number of physical cc registers")
    numIQEntries = Param.Unsigned(92, "Number of instruction queue entries")
    numROBEntries = Param.Unsigned(315, "Number of reorder buffer entries")

    smtNumFetchingThreads = Param.Unsigned(1, "SMT Number of Fetching Threads")
    smtFetchPolicy = Param.SMTFetchPolicy('RoundRobin', "SMT Fetch policy")
    smtLSQPolicy    = Param.SMTQueuePolicy('Partitioned',
                                           "SMT LSQ Sharing Policy")
    smtLSQThreshold = Param.Int(100, "SMT LSQ Threshold Sharing Parameter")
    smtIQPolicy    = Param.SMTQueuePolicy('Partitioned',
                                          "SMT IQ Sharing Policy")
    smtIQThreshold = Param.Int(100, "SMT IQ Threshold Sharing Parameter")
    smtROBPolicy   = Param.SMTQueuePolicy('Partitioned',
                                          "SMT ROB Sharing Policy")
    smtROBThreshold = Param.Int(100, "SMT ROB Threshold Sharing Parameter")
    smtCommitPolicy = Param.CommitPolicy('RoundRobin', "SMT Commit Policy")

    branchPred = Param.BranchPredictor(TAGE(numThreads =
                                                       Parent.numThreads),
                                       "Branch Predictor")

    #valuePred = Param.LastValuePredictor(LastValuePredictor(numThreads =
    #                                                   Parent.numThreads),
    valuePred = Param.VTAGEPredictor(VTAGEPredictor(numThreads =
                                                       Parent.numThreads),
                                       "Value Predictor")

    needsTSO = Param.Bool(buildEnv['TARGET_ISA'] == 'x86',
                          "Enable TSO Memory model")

    def addCheckerCpu(self):
        if buildEnv['TARGET_ISA'] in ['arm']:
            from m5.objects.ArmTLB import ArmMMU

            self.checker = O3Checker(workload=self.workload,
                                     exitOnError=False,
                                     updateOnError=True,
                                     warnOnlyOnLoadError=True)
            self.checker.mmu = ArmMMU()
            self.checker.mmu.itb.size = self.mmu.itb.size
            self.checker.mmu.dtb.size = self.mmu.dtb.size
            self.checker.cpu_id = self.cpu_id

        else:
            print("ERROR: Checker only supported under ARM ISA!")
            exit(1)
