/*
 * Copyright (c) 2016-2018,2019 ARM Limited
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder. You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2004-2005 The Regents of The University of Michigan
 * Copyright (c) 2013 Advanced Micro Devices, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu/o3/rename_map.hh"

#include <vector>

#include "cpu/reg_class.hh"
#include "debug/Rename.hh"

using namespace std;

/**** SimpleRenameMap methods ****/

SimpleRenameMap::SimpleRenameMap()
    : freeList(NULL), zeroReg(IntRegClass,0)
{
}


void
SimpleRenameMap::init(unsigned size, SimpleFreeList *_freeList,
                      RegIndex _zeroReg)
{
    assert(freeList == NULL);
    assert(map.empty());

    map.resize(size);
    zero_map.resize(size, false);
    extended.resize(8192, true);

    for (int i = 0; i < 256; i++)
    {
        extended[i] = false;
    }
    for (int i = 512; i < 768; i++)
    {
        // Small const 32-bit is not extended
        extended[i] = false;
    }
    freeList = _freeList;
    zeroReg = RegId(IntRegClass, _zeroReg);
}

SimpleRenameMap::RenameInfo
SimpleRenameMap::rename(const RegId& arch_reg,
                        const std::bitset<16> &idiom,
                        const std::bitset<16> &vp,
                        bool move,
                        PhysRegIdPtr mv_reg,
                        int width,
                        RegId const * src_arch_reg,
                        uint64_t ip_value,
                        const bool branch_zero_pred)
{
    PhysRegIdPtr renamed_reg;
    // Record the current physical register that is renamed to the
    // requested architected register.
    PhysRegIdPtr prev_reg = map[arch_reg.flatIndex()];

    DPRINTF(Rename, "Renaming %d ZeroReg: %d Idioms: "
        "%lu VP: %lu Move: %d Val: 0x%lx\n",
        arch_reg.flatIndex(),
        arch_reg == zeroReg,
        idiom.to_ullong(),
        vp.to_ullong(),
        move,
        ip_value);

    const bool arch_is_extended = width == 64;
    const bool previously_extended = mv_reg ? extended[mv_reg->index()] : true;
    // Can move elim if 64-bit into 64-bit
    // or 32-bit into 64-bit
    // or 32-bit into 32-bit
    // Cannot if 64-bit into 32-bit
    // But we can if the register we are moving is hardcoded or if we are
    // moving reg into itself
    const bool can_move_elim = move && mv_reg &&
        ((extended[mv_reg->index()] <= arch_is_extended) ||
        mv_reg->index() == 0);

        //((extended[mv_reg->index()] <= arch_is_extended) ||
        //(freeList->isHardwiredReg(mv_reg) && (mv_reg->index() < 256)));

    DPRINTF(Rename, "CanMove:%lu ExtendedRAT:%u Extended:%u "
        " hardWired:%u src is arch:%u\n",
        can_move_elim,
        mv_reg ? extended[mv_reg->flatIndex()] : true,
        arch_is_extended,
        mv_reg ? freeList->isHardwiredReg(mv_reg) : false,
        (src_arch_reg == &arch_reg));

    bool did_move_elim = false;

    if ((arch_reg == zeroReg) ||
        idiom.test(0) ||
        (vp.test(0) && !can_move_elim))
    {
        DPRINTF(Rename, "Dest is Zero Reg: %d\n",
             freeList->hardwiredPhyReg(0x0)->flatIndex());
        renamed_reg = freeList->hardwiredPhyReg(0);
        map[arch_reg.flatIndex()] = renamed_reg;
        zero_map[arch_reg.flatIndex()] = branch_zero_pred;
    }
    else if (idiom.any() || (vp.any() && !can_move_elim))
    {
        auto value = 0lu;

        if ((idiom.any() && !idiom.test(15)) ||
            (vp.any() && !vp.test(15)))
        {
            value = idiom.any() ?
                (idiom >> 1).to_ullong() :
                (vp >> 1).to_ullong();
        }
        else if (idiom.test(15) || vp.test(15))
        {
            value = ip_value;
            zero_map[arch_reg.flatIndex()] = branch_zero_pred;
        }

        DPRINTF(Rename, "Attempting to rename val: 0x%lx\n",
            value);
        DPRINTF(Rename, "Dest is hardwired Reg: %d\n",
            freeList->hardwiredPhyReg(value)->flatIndex());
        renamed_reg = freeList->hardwiredPhyReg(value);
        map[arch_reg.flatIndex()] = renamed_reg;
    }
    else if (can_move_elim)
    {
        assert(mv_reg->classValue() == IntRegClass);
        renamed_reg = mv_reg;
        DPRINTF(Rename, "Move elim, reusing %d\n",
            renamed_reg->flatIndex());
        freeList->reuseReg(renamed_reg);
        map[arch_reg.flatIndex()] = renamed_reg;
        did_move_elim = true;
        // Move register keeps same width
    }
    else if (prev_reg->getNumPinnedWrites() > 0) {
        // Do not rename if the register is pinned
        assert(arch_reg.getNumPinnedWrites() == 0);  // Prevent pinning the
                                                     // same register twice
        DPRINTF(Rename, "Renaming pinned reg, numPinnedWrites %d\n",
                prev_reg->getNumPinnedWrites());
        renamed_reg = prev_reg;
        renamed_reg->decrNumPinnedWrites();
    } else {
        // Failed alloc, give up
        if (move && !can_move_elim && freeList->numFreeRegs() == 0)
        {
            return RenameInfo(prev_reg,
                prev_reg,
                previously_extended,
                false, false);
        }
        else
        {
           renamed_reg = freeList->getReg();
           DPRINTF(Rename, "Dest is preg: %d\n", renamed_reg->flatIndex());
           map[arch_reg.flatIndex()] = renamed_reg;
           zero_map[arch_reg.flatIndex()] = false;
           extended[renamed_reg->index()] = arch_is_extended;
           renamed_reg->setNumPinnedWrites(arch_reg.getNumPinnedWrites());
           renamed_reg->setNumPinnedWritesToComplete(
                arch_reg.getNumPinnedWrites() + 1);
        }
    }

    DPRINTF(Rename, "Renamed reg %d to physical reg %d (%d) old mapping was"
            " %d (%d)\n",
            arch_reg, renamed_reg->flatIndex(), renamed_reg->flatIndex(),
            prev_reg->flatIndex(), prev_reg->flatIndex());

    return RenameInfo(renamed_reg,
        prev_reg,
        previously_extended,
        did_move_elim);
}


/**** UnifiedRenameMap methods ****/

void
UnifiedRenameMap::init(PhysRegFile *_regFile,
                       RegIndex _intZeroReg,
                       RegIndex _floatZeroReg,
                       UnifiedFreeList *freeList,
                       VecMode _mode)
{
    regFile = _regFile;
    vecMode = _mode;

    intMap.init(TheISA::NumIntRegs, &(freeList->intList), _intZeroReg);

    floatMap.init(TheISA::NumFloatRegs, &(freeList->floatList), _floatZeroReg);

    vecMap.init(TheISA::NumVecRegs, &(freeList->vecList), (RegIndex)-1);

    vecElemMap.init(TheISA::NumVecRegs * NVecElems,
            &(freeList->vecElemList), (RegIndex)-1);

    predMap.init(TheISA::NumVecPredRegs, &(freeList->predList), (RegIndex)-1);

    ccMap.init(TheISA::NumCCRegs, &(freeList->ccList), (RegIndex)-1);

}

void
UnifiedRenameMap::switchFreeList(UnifiedFreeList* freeList)
{
    if (vecMode == Enums::Elem) {

        /* The free list should currently be tracking full registers. */
        panic_if(freeList->hasFreeVecElems(),
                "The free list is already tracking Vec elems");
        panic_if(freeList->numFreeVecRegs() !=
                regFile->numVecPhysRegs() - TheISA::NumVecRegs,
                "The free list has lost vector registers");

        /* Split the free regs. */
        while (freeList->hasFreeVecRegs()) {
            auto vr = freeList->getVecReg();
            auto range = this->regFile->getRegElemIds(vr);
            freeList->addRegs(range.first, range.second);
        }

    } else if (vecMode == Enums::Full) {

        /* The free list should currently be tracking register elems. */
        panic_if(freeList->hasFreeVecRegs(),
                "The free list is already tracking full Vec");
        panic_if(freeList->numFreeVecElems() !=
                 regFile->numVecElemPhysRegs() -
                 TheISA::NumVecRegs * TheISA::NumVecElemPerVecReg,
                 "The free list has lost vector register elements");

        auto range = regFile->getRegIds(VecRegClass);
        freeList->addRegs(range.first + TheISA::NumVecRegs, range.second);

        /* We remove the elems from the free list. */
        while (freeList->hasFreeVecElems())
            freeList->getVecElem();
    }
}

void
UnifiedRenameMap::switchMode(VecMode newVecMode)
{
    if (newVecMode == Enums::Elem && vecMode == Enums::Full) {

        /* Switch to vector element rename mode. */
        vecMode = Enums::Elem;

        /* Split the mapping of each arch reg. */
        int vec_idx = 0;
        for (auto &vec: vecMap) {
            PhysRegFile::IdRange range = this->regFile->getRegElemIds(vec);
            auto idx = 0;
            for (auto phys_elem = range.first;
                 phys_elem < range.second; idx++, phys_elem++) {

                setEntry(RegId(VecElemClass, vec_idx, idx),
                    &(*phys_elem),
                    true);
            }
            vec_idx++;
        }

    } else if (newVecMode == Enums::Full && vecMode == Enums::Elem) {

        /* Switch to full vector register rename mode. */
        vecMode = Enums::Full;
        bool tmp = false;

        /* To rebuild the arch regs we take the easy road:
         *  1.- Stitch the elems together into vectors.
         *  2.- Replace the contents of the register file with the vectors
         *  3.- Set the remaining registers as free
         */
        TheISA::VecRegContainer new_RF[TheISA::NumVecRegs];
        for (uint32_t i = 0; i < TheISA::NumVecRegs; i++) {
            VecReg dst = new_RF[i].as<TheISA::VecElem>();
            for (uint32_t l = 0; l < NVecElems; l++) {
                RegId s_rid(VecElemClass, i, l);
                PhysRegIdPtr s_prid = vecElemMap.lookup(s_rid, tmp);
                dst[l] = regFile->readVecElem(s_prid);
            }
        }

        for (uint32_t i = 0; i < TheISA::NumVecRegs; i++) {
            PhysRegId pregId(VecRegClass, i, 0);
            regFile->setVecReg(regFile->getTrueId(&pregId), new_RF[i]);
        }

        auto range = regFile->getRegIds(VecRegClass);
        for (uint32_t i = 0; i < TheISA::NumVecRegs; i++) {
            setEntry(RegId(VecRegClass, i), &(*(range.first + i)), true);
        }

    }
}
