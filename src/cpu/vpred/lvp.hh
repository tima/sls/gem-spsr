/*
 * Copyright (c) 2020 CNRS
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#pragma once

#include "cpu/vpred/blacklisted.hh"
#include "params/LastValuePredictor.hh"

class LastValuePredictor : public BlacklistedPredictor
{
    struct Entry
    {
        uint64_t m_prediction = 0;
        uint64_t m_tag = 0x0;
        int m_conf = 0;

        Entry() = default;

        Entry(uint64_t val, uint64_t tag)
            : m_prediction(val)
            , m_tag(tag)
            , m_conf(0)
            {}
    };

  protected:
    uint64_t m_log2_sets;
    uint64_t m_index_mask;
    uint64_t m_tag_mask = 0xFFFF;

    std::vector<Entry> m_table;

  public:
    LastValuePredictor(const LastValuePredictorParams &params)
        : BlacklistedPredictor(params)
        , m_log2_sets(params.log2Sets)
        , m_index_mask((0x1 << m_log2_sets) - 1)
        , m_table(0x1 << m_log2_sets)
    {
    }

    virtual ~LastValuePredictor() = default;

    virtual uint64_t array_index(Addr inst_addr, int uop, int index) const
    {
        return ((inst_addr >> 2) ^ (index << 4) ^ (uop << 2)) & m_index_mask;
    }

    virtual uint64_t array_tag(Addr inst_addr) const
    {
        return ((inst_addr >> 2) >> m_log2_sets) & m_tag_mask;
    }

    virtual const std::string name() const
    override final
    {
      return BlacklistedPredictor::name();
    }

    virtual ValuePrediction derivedLookup(InstSeqNum sn, Addr inst_addr,
    ThreadID tid, int uop = 0, int index = 0) override;

    virtual void derivedSquash(InstSeqNum seq_num, ThreadID tid)
    override
    {
        // This is LVP, we are not doing anything
    }

    virtual bool derivedUpdate(InstSeqNum sn, Addr inst_addr,
    uint64_t value, bool idiom,
    ThreadID tid, int uop = 0, int index = 0) override;
};