/*
 * Copyright (c) 2014 ARM Limited
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu/vpred/lvp.hh"

#include "debug/ValuePred.hh"

typename ValuePredictor<O3CPUImpl>::ValuePrediction
LastValuePredictor::derivedLookup(
    InstSeqNum sn,
    Addr instr_addr,
    ThreadID tid,
    int uop,
    int index
)
{
    vpStats.numPredictions += (index == 0);

    const auto & entry =
        m_table[array_index(instr_addr, uop, index)];

    DPRINTF(
      ValuePred,
      "[sn:%lu] Using index %lu, tag %u\n",
      sn,
      array_index(instr_addr, uop, index),
      array_tag(instr_addr)
    );

    const bool tag_matches = entry.m_tag == array_tag(instr_addr);

    DPRINTF(
      ValuePred,
      "Tag matches: %u, got pred {%lu, %i}\n",
      tag_matches,
      entry.m_prediction,
      entry.m_conf
    );

    return {tag_matches && entry.m_conf >= satConf, entry.m_prediction};
}

bool LastValuePredictor::derivedUpdate(InstSeqNum sn, Addr inst_addr,
uint64_t value, bool idiom, ThreadID tid, int uop, int index)
{
    if (inst_addr == 0xdeadbeef)
    {
        return false;
    }

    bool blacklist = false;

    vpStats.numUpdates++;

    auto & entry =
        m_table[array_index(inst_addr, uop, index)];

    const bool tag_matches = entry.m_tag == array_tag(inst_addr);

    DPRINTF(
      ValuePred,
      "[sn:%lu] Updating entry %lu: Entry has 0x%lx, got 0x%lx\n",
      sn,
      array_index(inst_addr, uop, index),
      entry.m_prediction,
      value
    );

    if (!tag_matches && (((value >= 2 && value != 8 && value != 16)
        && !enableFullVP) || idiom))
    {
        // Do not overwrite an entry if we are not
        // getting 0x0 or 0x1 or 0x2 or 0x4 as a value
        return false;
    }

    if (tag_matches)
    {
        entry.m_conf += (entry.m_conf < satConf) &&
            (entry.m_prediction == value);

        const bool discard = (value >= 2 &&
            value != 8 && value != 16);

        if (entry.m_prediction != value || discard)
        {
            blacklist = entry.m_conf == satConf;
            entry.m_conf = 0;
        }

        vpStats.numCorrectPredictions += entry.m_prediction == value;
        vpStats.numIncorrectPredictions += entry.m_prediction != value;

        if (entry.m_conf == 0)
        {
            if (enableFullVP || (value <= 1) || value == 8 || value == 16)
            {
                entry.m_prediction = value;
            }
        }
        DPRINTF(
          ValuePred,
          "[sn:%lu] Tag matches, entry now has {%lu, %i}\n",
          sn,
          entry.m_prediction,
          entry.m_conf
        );
    }
    else
    {
       entry.m_conf = std::max(entry.m_conf - 8, 0);
       if (entry.m_conf == 0)
       {
           ++vpStats.numReplacements;
           entry = {value, array_tag(inst_addr)};
       }
       DPRINTF(
         ValuePred,
         "[sn:%lu] Tag does not matches, entry now has {%lu, %u, %i}\n",
         sn,
         entry.m_prediction,
         array_tag(inst_addr),
         entry.m_conf
        );
    }

    return blacklist;
}

LastValuePredictor *
LastValuePredictorParams::create() const
{
    return new LastValuePredictor(*this);
}
