/*
 * Copyright (c) 2020 CNRS
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#pragma once

#include <map>
#include <vector>

#include "arch/types.hh"
#include "base/statistics.hh"
#include "config/the_isa.hh"
#include "cpu/inst_seq.hh"
#include "sim/sim_object.hh"

template <class Impl>
class ValuePredictor
{
  public:
    struct ValuePrediction
    {
      bool predict = false;
      uint64_t prediction = 0xdeadbeef;

      ValuePrediction(bool pred, uint64_t pred_value)
       : predict(pred)
       , prediction(pred_value)
       {}
    };

    struct ValuePredictionInfo
    {
      typename Impl::DynInstPtr instr;
      bool did_predict;
      uint64_t prediction;
      int uop;
      int index;
      void * history = nullptr;
      InstSeqNum sn;

      ValuePredictionInfo(const typename Impl::DynInstPtr & instruction,
        bool predict, uint64_t pred, int uopidx, int idx, InstSeqNum seq)
        : instr(instruction)
        , did_predict(predict)
        , prediction(pred)
        , uop(uopidx)
        , index(idx)
        , sn(seq)
        {}
    };

  protected:
    std::deque<ValuePredictionInfo> m_history;

    bool enableFullVP;
    bool enableHalfVP;
    InstSeqNum expected_gated_sn = 0;

  public:
    ValuePredictor(bool enable_fvp, bool enable_hvp);

    virtual ~ValuePredictor();

    virtual const std::string name() const
    {
      return ".valuePred";
    }

    ValuePrediction lookup(const typename Impl::DynInstPtr & instr,
    InstSeqNum sn, int uop = 0, int index = 0);

    virtual ValuePrediction filteredLookup(InstSeqNum sn, Addr inst_addr,
    ThreadID tid, int uop = 0, int index = 0) = 0;

    virtual ValuePrediction derivedLookup(InstSeqNum sn, Addr inst_addr,
    ThreadID tid, int uop = 0, int index = 0) = 0;

    virtual void updateBranch(
      InstSeqNum sn,
      Addr pc,
      bool direction,
      Addr target,
      ThreadID tid,
      bool uncond = false
    );

    virtual void fixupBranch(
      InstSeqNum sn,
      Addr pc,
      bool direction,
      Addr target,
      ThreadID tid,
      bool uncond = false
    );

    void squash(InstSeqNum seq_num, ThreadID tid);

    virtual void derivedSquash(InstSeqNum seq_num, ThreadID tid) = 0;

    void update(InstSeqNum seq_num, ThreadID tid);

    virtual void filteredUpdate(InstSeqNum sn, bool mispred, bool correct,
    bool idiom,
    Addr inst_addr, uint64_t value, ThreadID tid,
    int uop = 0, int index = 0) = 0;

    virtual bool derivedUpdate(InstSeqNum sn, Addr inst_addr,
    uint64_t value, bool idiom, ThreadID tid, int uop = 0, int index = 0) = 0;

    void gate(InstSeqNum sn, Addr addr);

    bool ungate(InstSeqNum sn = 0);
};