/*
 * Copyright (c) 2014 ARM Limited
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu/vpred/blacklisted.hh"

#include <algorithm>

#include "cpu/utils.hh"
#include "debug/ValuePred.hh"

BlacklistedPredictor::VPStatGroup::VPStatGroup(Stats::Group * parent)
: Stats::Group(parent)
, ADD_STAT(numPredictions, "Number of VP queries")
, ADD_STAT(numBlacklisted, "Number of blacklisted inst")
, ADD_STAT(numPredIgnoredBlacklist, "Number of no pred because blacklisted")
, ADD_STAT(numUpdates, "Number of VP training")
, ADD_STAT(numReplacements, "Number of replacements")
, ADD_STAT(numCorrectPredictions,
    "Number of correct predictions (not necessarily confident")
, ADD_STAT(numIncorrectPredictions,
    "Number of incorrect predictions (not necessarily confident")
, ADD_STAT(numUsedCorrectPredictions, "Number of confident correct preds")
, ADD_STAT(numUsedIncorrectPredictions, "Number of confident mispreds")
{
}

typename ValuePredictor<O3CPUImpl>::ValuePrediction
BlacklistedPredictor::filteredLookup(
    InstSeqNum sn,
    Addr instr_addr,
    ThreadID tid,
    int uop,
    int index
)
{
    auto vpred = derivedLookup(sn, instr_addr, tid, uop, index);

    if (enableBlacklist &&
    m_blacklist_map.count(
      generateBlacklistID(
        instr_addr,
        uop,
        index
      )
    ) != 0)
    {
        vpred.predict = false;
        vpStats.numPredIgnoredBlacklist++;
        DPRINTF(ValuePred, "[sn:%lu] Blacklisted\n", sn);
    }

    return vpred;
}

void BlacklistedPredictor::filteredUpdate(InstSeqNum sn, bool mispred,
  bool correct, bool idiom,
  Addr inst_addr, uint64_t value, ThreadID tid, int uop, int index)
{
    vpStats.numUsedIncorrectPredictions += mispred;
    vpStats.numUsedCorrectPredictions += correct;

    bool blacklist = derivedUpdate(
      sn,
      inst_addr,
      value,
      idiom,
      tid,
      uop,
      index
    );

    DPRINTF(ValuePred, "[sn:%lu] Blacklist: %u\n", sn, blacklist);

    if (enableBlacklist && blacklist)
    {
      updateBlacklist(generateBlacklistID(inst_addr, uop, index));
    }
}

void BlacklistedPredictor::updateBlacklist(Addr inst_addr)
{
  auto it = std::find_if(
    m_potential_blacklist.begin(),
    m_potential_blacklist.end(),
    [inst_addr](const auto & entry)
    {
      return entry.pc == inst_addr;
    }
  );

  if (it != m_potential_blacklist.end())
  {
    DPRINTF(ValuePred, "0x%lx has %lu mispred in TBL\n",
      inst_addr,
      it->mispred_count);
    if (++it->mispred_count > m_blacklist_threshold)
    {
      if (moveToBlacklist(it))
      {
        m_potential_blacklist.erase(it);
      }
    }
  }
  else
  {
    if (m_potential_blacklist.size() == m_pot_blacklist_size)
    {

      DPRINTF(ValuePred, "PC 0x%lx : TBL is full\n",
        inst_addr);
      auto & entry = m_potential_blacklist.front();
      if (entry.mispred_count == 0)
      {
        // std::cout << std::hex << inst_addr;
        // std::cout << " -- Inserting in front" << std::endl;
        DPRINTF(ValuePred, "Inserting 0x%lx in front\n",
          inst_addr);
        entry = PotentialBlackListEntry{inst_addr};
      }
      else
      {
        // std::cout << std::hex << inst_addr;
        // std::cout << " -- Updating other's count" << std::endl;
        DPRINTF(ValuePred, "PC 0x%lx, updating other counts\n",
          inst_addr);
        std::for_each(
          m_potential_blacklist.begin(),
          m_potential_blacklist.end(),
          [this](auto & to_upd)
          {
            to_upd.mispred_count >>= 1;
            // std::cout << std::hex << to_upd.pc;
            // std::cout << " -- Now " << std::dec;
            // std::cout << to_upd.mispred_count << std::endl;
            DPRINTF(ValuePred, "PC 0x%lx now has count %lu\n",
              to_upd.pc, to_upd.mispred_count);
          }
        );
      }
    }
    else
    {
    //   std::cout << std::hex << inst_addr;
    //   std::cout << " -- Inserting not full" << std::endl;
      DPRINTF(ValuePred, "Inserting 0x%lx in TBL\n",
        inst_addr);
      m_potential_blacklist.emplace_back(inst_addr);
    }
  }

  std::sort(
    m_potential_blacklist.begin(),
    m_potential_blacklist.end(),
    [](const auto & lhs, const auto & rhs)
    {
      return lhs.mispred_count < rhs.mispred_count;
    }
  );
}

bool BlacklistedPredictor::moveToBlacklist(
    const typename std::vector<PotentialBlackListEntry>::iterator & it)
{
  if (m_blacklist_vec.size() == m_blacklist_size)
  {
    auto time = curTick() - m_blacklist_vec.front().added_at;
    if (time > (333 * 10000))
    {
      DPRINTF(ValuePred, "Moving PC 0x%lx to BL\n", it->pc);
      DPRINTF(ValuePred, "Blacklist has %lu elems\n",
        m_blacklist_vec.size());

      vpStats.numBlacklisted++;
      m_blacklist_map.erase(m_blacklist_vec.front().pc);
      m_blacklist_vec.pop_front();
      m_blacklist_vec.emplace_back(it->pc, curTick());
      m_blacklist_map.emplace(it->pc, BlackListEntry{it->pc, curTick()});

#ifndef NDEBUG
      for (auto & entry : m_blacklist_vec)
      {
          DPRINTF(ValuePred, "{0x%lx, added: %lu}\n",
            entry.pc,
            entry.added_at);
      }
#endif
    }
    else
    {
      DPRINTF(ValuePred, "No space in BL, giving up\n");
      return false;
    }
  }
  else
  {
    if (m_blacklist_map.count(it->pc) != 0)
    {
        DPRINTF(ValuePred, "PC 0x%lx Already present,updating\n",
          it->pc);
        auto itvec = std::find_if(m_blacklist_vec.begin(),
        m_blacklist_vec.end(), [&it](const auto & entry)
        {
            return entry.pc == it->pc;
        });

        assert(itvec != m_blacklist_vec.end());
        m_blacklist_vec.emplace_back(it->pc, curTick());
        m_blacklist_vec.erase(itvec);
    }
    else
    {

       DPRINTF(ValuePred, "Moving PC 0x%lx to BL\n", it->pc);
       DPRINTF(ValuePred, "Blacklist has %lu elems\n",
        m_blacklist_vec.size());

        vpStats.numBlacklisted++;
        m_blacklist_vec.emplace_back(it->pc, curTick());
        m_blacklist_map.emplace(
          it->pc, BlackListEntry{it->pc,
          curTick()
        });
#ifndef NDEBUG
        for (auto & entry : m_blacklist_vec)
      {
          DPRINTF(ValuePred, "{0x%lx, added: %lu}\n",
            entry.pc,
            entry.added_at);
      }
#endif
    }
  }

  return true;
}
