# Copyright (c) 2012 Mark D. Hill and David A. Wood
# Copyright (c) 2015 The University of Wisconsin
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from m5.SimObject import SimObject
from m5.params import *
from m5.proxy import *

class BlacklistedPredictor(SimObject):
    type = 'BlacklistedPredictor'
    cxx_header = "cpu/vpred/blacklisted.hh"
    abstract = True

    numThreads = Param.Unsigned(1, "Number of threads")
    enableBlacklist = Param.Unsigned(0, "Enable blacklist")
    enableFullVP = Param.Unsigned(0, "Predict all values")
    enableHalfVP = Param.Unsigned(0, "Predict 9-bit signed values")
    instShiftAmt = Param.Unsigned(2, "PC shift")
    satConf = Param.Unsigned(7, "Sat conf")
    incProba = Param.Unsigned(16, "FPC proba")

class LastValuePredictor(BlacklistedPredictor):
    type = 'LastValuePredictor'
    cxx_header = "cpu/vpred/lvp.hh"

    log2Sets = Param.Unsigned(13, "Number of LVP array sets")


class GShareValuePredictor(LastValuePredictor):
    type = 'GShareValuePredictor'
    cxx_header = "cpu/vpred/gshare.hh"
    ghist_length = Param.Unsigned(8, "length of ghist")

class VTAGEBase(SimObject):
    type = 'VTAGEBase'
    cxx_class = 'VTAGEBase'
    cxx_header = "cpu/vpred/vtage_base.hh"

    satConf = Param.Unsigned(Parent.satConf, "Sat conf")
    incProba = Param.Unsigned(Parent.incProba, "FPC Proba")
    enableFullVP = Param.Unsigned(Parent.enableFullVP,
        "Predict all values")
    enableHalfVP = Param.Unsigned(Parent.enableHalfVP,
        "Predict 9-bit signed values")

    numThreads = Param.Unsigned(Parent.numThreads, "Number of threads")
    instShiftAmt = Param.Unsigned(Parent.instShiftAmt,
        "Number of bits to shift instructions by")

    #nHistoryTables = Param.Unsigned(11, "Number of history tables")
    nHistoryTables = Param.Unsigned(7, "Number of history tables")
    minHist = Param.Unsigned(2, "Minimum history size of TAGE")
    maxHist = Param.Unsigned(128, "Maximum history size of TAGE")

    tagTableTagWidths = VectorParam.Unsigned(
        #[0, 9, 9, 10, 10, 11, 11, 12, 12, 12, 13, 13],
        [0, 9, 9, 10, 10, 11, 11, 12],
        "Tag size in TAGE tag tables")
    logTagTableSizes = VectorParam.Int(
        #[12, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 8],
        [12, 9, 9, 8, 8, 8, 7, 7],
        "Log2 of TAGE table sizes")
    logRatioBiModalHystEntries = Param.Unsigned(2,
        "Log num of prediction entries for a shared hysteresis bit " \
        "for the Bimodal")

    tagTableCounterBits = Param.Unsigned(3, "Number of tag table counter bits")
    tagTableUBits = Param.Unsigned(2, "Number of tag table u bits")

    histBufferSize = Param.Unsigned(2097152,
            "A large number to track all branch histories(2MEntries default)")

    pathHistBits = Param.Unsigned(16, "Path history size")
    logUResetPeriod = Param.Unsigned(18,
        "Log period in number of branches to reset TAGE useful counters")
    numUseAltOnNa = Param.Unsigned(1, "Number of USE_ALT_ON_NA counters")
    initialTCounterValue = Param.Int(1 << 17, "Initial value of tCounter")
    useAltOnNaBits = Param.Unsigned(4, "Size of the USE_ALT_ON_NA counter(s)")

    maxNumAlloc = Param.Unsigned(1,
        "Max number of TAGE entries allocted on mispredict")

    # List of enabled TAGE tables. If empty, all are enabled
    noSkip = VectorParam.Bool([], "Vector of enabled TAGE tables")

    speculativeHistUpdate = Param.Bool(True,
        "Use speculative update for histories")

# TAGE branch predictor as described in https://www.jilp.org/vol8/v8paper1.pdf
# The default sizes below are for the 8C-TAGE configuration (63.5 Kbits)
class VTAGEPredictor(BlacklistedPredictor):
    type = 'VTAGEPredictor'
    cxx_class = 'VTAGEPredictor'
    cxx_header = "cpu/vpred/vtage.hh"
    vtage = Param.VTAGEBase(VTAGEBase(), "VTage object")
