/*
 * Copyright (c) 2014 ARM Limited
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu/vpred/vp.hh"

#include <algorithm>

#include "cpu/o3/dyn_inst.hh"
#include "cpu/o3/impl.hh"
#include "debug/ValuePred.hh"

template <class Impl>
ValuePredictor<Impl>::ValuePredictor(bool enable_fvp, bool enable_hvp)
 : enableFullVP(enable_fvp)
 , enableHalfVP(enable_hvp)
{}

template <class Impl>
ValuePredictor<Impl>::~ValuePredictor() {}

template <class Impl>
typename ValuePredictor<Impl>::ValuePrediction
ValuePredictor<Impl>::lookup(const typename Impl::DynInstPtr & instr,
InstSeqNum sn, int uop, int index)
{
  const auto vpred = filteredLookup(
    instr->seqNum,
    instr->pcState().instAddr(),
    instr->threadNumber,
    uop,
    index
  );

  m_history.emplace_back(
    instr, vpred.predict,
    vpred.prediction, uop, index, instr->seqNum
  );

  DPRINTF(ValuePred, "Pushing history for [sn:%llu]\n",
    instr->seqNum);
  return vpred;
}

template <class Impl>
void ValuePredictor<Impl>::update(InstSeqNum seq_num, ThreadID tid)
{
  while (!m_history.empty() && m_history.front().sn <= seq_num)
  {
    DPRINTF(ValuePred, "Updating for [sn:%llu]\n",
     m_history.front().sn);
    Addr instr_addr = m_history.front().instr ?
      m_history.front().instr->pcState().instAddr() :
      0xfeedbeef;

    uint64_t instr_result = m_history.front().instr ?
      m_history.front().instr->readIntOutRegOperand(
      m_history.front().instr->staticInst.get(),
      m_history.front().index,
      //!enableFullVP
      true
    ) : 0xdeadbeef;

    const bool is_pred = m_history.front().instr ?
      m_history.front().instr->
        isValuePredictedIdx(m_history.front().index) :
        false;

    const bool is_mispred = is_pred ?
      m_history.front().instr->isVPGated() :
      false;


    const bool is_dyn_idiom = false;
      // This yields better performance, counter intuitively
      /*m_history.front().instr ?
      (m_history.front().instr->isDynZeroIdiom() ||
      m_history.front().instr->isDynOneIdiom()) : false;*/

    filteredUpdate(
      m_history.front().sn,
      is_mispred,
      is_pred,
      is_dyn_idiom,
      instr_addr,
      instr_result,
      tid,
      m_history.front().uop,
      m_history.front().index
    );
    m_history.pop_front();
  }
}

template <class Impl>
void ValuePredictor<Impl>::updateBranch(
  InstSeqNum sn,
  Addr pc,
  bool direction,
  Addr target,
  ThreadID tid,
  bool uncond
)
{
  m_history.emplace_back(
    nullptr, false, 0xdeadbeef, 0, 0, sn
  );
  // Nothing to do, just here for predictors that don't have history
}

template <class Impl>
void ValuePredictor<Impl>::fixupBranch(
  InstSeqNum sn,
  Addr pc,
  bool direction,
  Addr target,
  ThreadID tid,
  bool uncond
)
{
}

template <class Impl>
void ValuePredictor<Impl>::squash(InstSeqNum seq_num, ThreadID tid)
{
    DPRINTF(ValuePred, "Squash until [sn:%lu]\n", seq_num);
    while (!m_history.empty() && m_history.back().sn > seq_num)
    {
      DPRINTF(ValuePred, "Removing history for [sn:%llu]\n",
        m_history.back().sn);
        m_history.pop_back();
    }
    derivedSquash(seq_num, tid);
}

template <class Impl>
bool ValuePredictor<Impl>::ungate(InstSeqNum sn)
{
  if (sn < expected_gated_sn)
  {
    expected_gated_sn = 0;
    return true;
  }
  return false;
}

template <class Impl>
void ValuePredictor<Impl>::gate(InstSeqNum sn, Addr addr)
{
  if (sn < expected_gated_sn)
  {
    expected_gated_sn = sn;

    DPRINTF(ValuePred, "Gating addr %lx\n", addr);
  }
}

template class ValuePredictor<O3CPUImpl>;
