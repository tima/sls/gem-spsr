/*
 * Copyright (c) 2014 The University of Wisconsin
 *
 * Copyright (c) 2006 INRIA (Institut National de Recherche en
 * Informatique et en Automatique  / French National Research Institute
 * for Computer Science and Applied Mathematics)
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* @file
 * Implementation of a VTAGEPredictor value predictor
 */

#include "cpu/vpred/vtage.hh"

#include "base/intmath.hh"
#include "base/logging.hh"
#include "base/trace.hh"
#include "debug/ValuePred.hh"

VTAGEPredictor::VTAGEPredictor(const VTAGEPredictorParams &params)
: BlacklistedPredictor(params)
, vtage(params.vtage)
{
}

// PREDICTOR UPDATE
/*void
VTAGEPredictor::update(ThreadID tid, Addr branch_pc,
bool taken, void* bp_history,
bool squashed, const StaticInstPtr & inst, Addr corrTarget)
{
    assert(bp_history);

    VTageInfo *bi = static_cast<VTageInfo*>(bp_history);
    VTAGEPredictorBase::Info *bi = bi->vtageInfo;

    if (squashed) {
        // This restores the global history, then update it
        // and recomputes the folded histories.
        VTAGEPredictor->squash(tid, taken, bi, corrTarget);
        return;
    }

    int nrand = random_mt.random<int>() & 3;
    if (bi->vtageInfo->condBranch) {
        DPRINTF(ValuePred, "Updating tables for branch:%lx; taken?:%d\n",
                branch_pc, taken);
        vtage->updateStats(taken, bi->vtageInfo);
        vtage->condBranchUpdate(tid, branch_pc, taken, bi, nrand,
                               corrTarget, bi->vtageInfo->VTAGEPredictorPred);
    }

    // optional non speculative update of the histories
    vtage->updateHistories(tid, branch_pc, taken, bi, false, inst,
                          corrTarget);
    delete bi;
}*/

void
VTAGEPredictor::derivedSquash(InstSeqNum seq_num, ThreadID tid)
{
    while (!m_history.empty() && m_history.back()->sn > seq_num)
    {
        auto * bp_history = m_history.back();
        auto *bi = bp_history->vtageInfo;

        DPRINTF(ValuePred, "Deleting vp info: [sn:%lu]\n",
            bp_history->sn);

        // Always restore branch history to make
        // it simple
        if (bi->branch)
        {
            vtage->restoreHistories(tid, bi);
        }

        delete bp_history;
        m_history.pop_back();
    }
}

typename ValuePredictor<O3CPUImpl>::ValuePrediction
VTAGEPredictor::derivedLookup(InstSeqNum sn, Addr inst_addr,
ThreadID tid, int uop, int index)
{
    vpStats.numPredictions += (index == 0);

    DPRINTF(ValuePred, "Predicting for [sn:%lu]\n", sn);
    VTageInfo *bi = new VTageInfo(*vtage, sn);
    m_history.push_back(bi);
    return vtage->vtagePredict(
        tid,
        array_index(inst_addr, uop, index),
        bi->vtageInfo
    );
}

void
VTAGEPredictor::updateBranch(
    InstSeqNum sn,
    Addr pc,
    bool direction,
    Addr target,
    ThreadID tid,
    bool uncond
)
{
    // To create the history and trigger the
    // update
    ValuePredictor<O3CPUImpl>::updateBranch(
        sn,
        pc,
        direction,
        target,
        tid,
        uncond
    );

    VTageInfo *info = new VTageInfo(*vtage, sn);
    auto * bi = info->vtageInfo;
    bi->branch = true;
    m_history.push_back(info);
    DPRINTF(ValuePred, "Updating history for [sn:%lu]\n",
        sn);
    vtage->updateHistories(tid, pc, direction, bi);
}

void
VTAGEPredictor::fixupBranch(
    InstSeqNum sn,
    Addr pc,
    bool direction,
    Addr target,
    ThreadID tid,
    bool uncond
)
{
    assert(!m_history.empty());

    if (m_history.back()->sn != sn)
    {
        DPRINTF(ValuePred, "Expecting history of [sn:%lu], got [sn:%lu]\n",
            sn,
            m_history.back()->sn);
    }

    assert(m_history.back()->sn == sn);

    DPRINTF(ValuePred, "Fixing up branch history for [sn:%lu]\n", sn);
    auto * bi = m_history.back()->vtageInfo;
    vtage->restoreHistories(tid, bi);
    vtage->updateHistories(tid, pc, direction, bi);
}

bool
VTAGEPredictor::derivedUpdate(InstSeqNum sn, Addr inst_addr,
uint64_t value, bool idiom, ThreadID tid, int uop, int index)
{
    auto * bp_history = m_history.front();
    DPRINTF(ValuePred, "Updating for [sn:%lu] PC 0x%lx, "
        " history has [sn:%lu]\n",
        inst_addr,
        sn,
        bp_history->sn);

    assert (bp_history->sn == sn);

    VTAGEBase::Info *bi = bp_history->vtageInfo;

    /*if (squashed) {
        // This restores the global history, then update it
        // and recomputes the folded histories.
        VTAGEPredictor->squash(tid, taken, bi, corrTarget);
        return;
    }*/

    int nrand = my_random.random<int>() & 3;
    bool blacklist = false;
    if (!bi->branch) {
        vpStats.numUpdates++;
        DPRINTF(ValuePred, "Updating tables for "
            "[sn:%lu]; Res: 0x%lx\n",
            sn,
            value);
        vtage->updateStats(value, bi);
        blacklist = vtage->updatePredictor(tid, bi->pc, value, bi, nrand,
                                value /* don't care */, idiom);
    }

    // optional non speculative update of the histories
    /*if (bi->branch)
    {
        vtage->updateHistories(tid, bi->pc, taken,
            bi, false, inst, corrTarget);
    }*/

    delete bp_history;
    m_history.pop_front();

    // TODO Deal with blacklist later
    return blacklist;
}

/*void
VTAGEPredictor::uncondBranch(ThreadID tid, Addr br_pc, void* &bp_history)
{
    DPRINTF(ValuePred, "UnConditionalBranch: %lx\n", br_pc);
    predict(tid, br_pc, false, bp_history);
    VTageInfo *bi = static_cast<VTageInfo*>(bp_history);
    vtage->updateHistories(tid, br_pc, true, bi->vtageInfo, true);
}*/

VTAGEPredictor *
VTAGEPredictorParams::create() const
{
    return new VTAGEPredictor(*this);
}
