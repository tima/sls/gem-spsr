/*
 * Copyright (c) 2014 The University of Wisconsin
 *
 * Copyright (c) 2006 INRIA (Institut National de Recherche en
 * Informatique et en Automatique  / French National Research Institute
 * for Computer Science and Applied Mathematics)
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* @file
 * Implementation of a TAGE branch predictor. TAGE is a global-history based
 * branch predictor. It features a PC-indexed bimodal predictor and N
 * partially tagged tables, indexed with a hash of the PC and the global
 * branch history. The different lengths of global branch history used to
 * index the partially tagged tables grow geometrically. A small path history
 * is also used in the hash.
 *
 * All TAGE tables are accessed in parallel, and the one using the longest
 * history that matches provides the prediction (some exceptions apply).
 * Entries are allocated in components using a longer history than the
 * one that predicted when the prediction is incorrect.
 */

#pragma once

#include <vector>

#include "base/random.hh"
#include "base/types.hh"
#include "cpu/vpred/blacklisted.hh"
#include "cpu/vpred/vtage_base.hh"
#include "params/VTAGEPredictor.hh"

class VTAGEPredictor : public BlacklistedPredictor
{
  protected:
    VTAGEBase *vtage;

    struct VTageInfo {
        VTAGEBase::Info *vtageInfo;
        InstSeqNum sn = 0;

        VTageInfo(VTAGEBase &vtage, InstSeqNum seqNum)
        : vtageInfo(vtage.makeInfo())
        , sn(seqNum)
        {}

        virtual ~VTageInfo()
        {
            delete vtageInfo;
        }
    };

    std::deque<VTageInfo*> m_history;

  public:

    Random my_random;

    VTAGEPredictor(const VTAGEPredictorParams &params);

    virtual ~VTAGEPredictor() = default;

    // Base class methods.
    //void uncondBranch(ThreadID tid, Addr br_pc, void* &bp_history);
    /*void update(ThreadID tid, Addr branch_addr, bool taken, void *bp_history,
                bool squashed, const StaticInstPtr & inst,
                Addr corrTarget);*/

    virtual void derivedSquash(
      InstSeqNum seq_num,
      ThreadID tid
    ) override final;

    virtual ValuePrediction derivedLookup(InstSeqNum sn, Addr inst_addr,
    ThreadID tid, int uop = 0, int index = 0) override final;

    virtual bool derivedUpdate(InstSeqNum sn, Addr inst_addr,
    uint64_t value, bool idiom,
    ThreadID tid, int uop = 0, int index = 0) override final;

    uint64_t array_index(Addr inst_addr, int uop, int index) const
    {
        return ((inst_addr >> 2) ^ (index << 4) ^ (uop << 2));
    }

    virtual void updateBranch(
      InstSeqNum sn,
      Addr pc,
      bool direction,
      Addr target,
      ThreadID tid,
      bool uncond = false
    ) override;

      virtual void fixupBranch(
      InstSeqNum sn,
      Addr pc,
      bool direction,
      Addr target,
      ThreadID tid,
      bool uncond = false
    ) override;


};
