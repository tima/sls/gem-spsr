/*
 * Copyright (c) 2014 The University of Wisconsin
 *
 * Copyright (c) 2006 INRIA (Institut National de Recherche en
 * Informatique et en Automatique  / French National Research Institute
 * for Computer Science and Applied Mathematics)
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* @file
 * Implementation of a TAGE branch predictor
 */

#include "cpu/vpred/vtage_base.hh"

#include "base/intmath.hh"
#include "base/logging.hh"
#include "debug/ValuePred.hh"

VTAGEBase::VTAGEBase(const VTAGEBaseParams &p)
   : SimObject(p),
     logRatioBiModalHystEntries(p.logRatioBiModalHystEntries),
     nHistoryTables(p.nHistoryTables),
     tagTableCounterBits(p.tagTableCounterBits),
     tagTableUBits(p.tagTableUBits),
     histBufferSize(p.histBufferSize),
     minHist(p.minHist),
     maxHist(p.maxHist),
     pathHistBits(p.pathHistBits),
     satConf(p.satConf),
     proba(p.incProba),
     enableFullVP(p.enableFullVP),
     enableHalfVP(p.enableHalfVP),
     tagTableTagWidths(p.tagTableTagWidths),
     logTagTableSizes(p.logTagTableSizes),
     threadHistory(p.numThreads),
     logUResetPeriod(p.logUResetPeriod),
     initialTCounterValue(p.initialTCounterValue),
     numUseAltOnNa(p.numUseAltOnNa),
     useAltOnNaBits(p.useAltOnNaBits),
     maxNumAlloc(p.maxNumAlloc),
     noSkip(p.noSkip),
     speculativeHistUpdate(p.speculativeHistUpdate),
     instShiftAmt(p.instShiftAmt),
     initialized(false),
     stats(this, nHistoryTables)
{
    if (noSkip.empty()) {
        // Set all the table to enabled by default
        noSkip.resize(nHistoryTables + 1, true);
    }

}

VTAGEBase::Info*
VTAGEBase::makeInfo() {
    return new Info(*this);
}

void
VTAGEBase::init()
{
    if (initialized) {
       return;
    }

    // Current method for periodically resetting the u counter bits only
    // works for 1 or 2 bits
    // Also make sure that it is not 0
    assert(tagTableUBits <= 2 && (tagTableUBits > 0));

    // we use int type for the path history, so it cannot be more than
    // its size
    assert(pathHistBits <= (sizeof(int)*8));

    // initialize the counter to half of the period
    assert(logUResetPeriod != 0);
    tCounter = initialTCounterValue;

    assert(histBufferSize > maxHist * 2);

    useAltPredForNewlyAllocated.resize(numUseAltOnNa, 0);

    for (auto& history : threadHistory) {
        history.pathHist = 0;
        history.globalHistory = new uint8_t[histBufferSize];
        history.gHist = history.globalHistory;
        memset(history.gHist, 0, histBufferSize);
        history.ptGhist = 0;
    }

    histLengths = new int [nHistoryTables+1];

    calculateParameters();

    assert(tagTableTagWidths.size() == (nHistoryTables+1));
    assert(logTagTableSizes.size() == (nHistoryTables+1));

    // First entry is for the Bimodal table and it is untagged in this
    // implementation
    assert(tagTableTagWidths[0] == 0);

    for (auto& history : threadHistory) {
        history.computeIndices = new FoldedHistory[nHistoryTables+1];
        history.computeTags[0] = new FoldedHistory[nHistoryTables+1];
        history.computeTags[1] = new FoldedHistory[nHistoryTables+1];

        initFoldedHistories(history);
    }

    const uint64_t bimodalTableSize = ULL(1) << logTagTableSizes[0];
    btablePrediction.resize(bimodalTableSize);
    btableHysteresis.resize(bimodalTableSize >> logRatioBiModalHystEntries,
                            true);

    gtable = new VTageEntry*[nHistoryTables + 1];
    buildTageTables();

    tableIndices = new int [nHistoryTables+1];
    tableTags = new int [nHistoryTables+1];
    initialized = true;
}

void
VTAGEBase::initFoldedHistories(ThreadHistory & history)
{
    for (int i = 1; i <= nHistoryTables; i++) {
        history.computeIndices[i].init(
            histLengths[i], (logTagTableSizes[i]));
        history.computeTags[0][i].init(
            history.computeIndices[i].origLength, tagTableTagWidths[i]);
        history.computeTags[1][i].init(
            history.computeIndices[i].origLength, tagTableTagWidths[i]-1);
        DPRINTF(ValuePred, "HistLength:%d, TTSize:%d, TTTWidth:%d\n",
                histLengths[i], logTagTableSizes[i], tagTableTagWidths[i]);
    }
}

void
VTAGEBase::buildTageTables()
{
    for (int i = 1; i <= nHistoryTables; i++) {
        gtable[i] = new VTageEntry[1<<(logTagTableSizes[i])];
    }
}

void
VTAGEBase::calculateParameters()
{
    histLengths[1] = minHist;
    histLengths[nHistoryTables] = maxHist;

    for (int i = 2; i <= nHistoryTables; i++) {
        histLengths[i] = (int) (((double) minHist *
                       pow ((double) (maxHist) / (double) minHist,
                           (double) (i - 1) / (double) ((nHistoryTables- 1))))
                       + 0.5);
    }
}

void
VTAGEBase::btbUpdate(ThreadID tid, Addr branch_pc, Info* &bi)
{
    if (speculativeHistUpdate) {
        ThreadHistory& tHist = threadHistory[tid];
        DPRINTF(ValuePred, "BTB miss resets prediction: %lx\n", branch_pc);
        assert(tHist.gHist == &tHist.globalHistory[tHist.ptGhist]);
        tHist.gHist[0] = 0;
        for (int i = 1; i <= nHistoryTables; i++) {
            tHist.computeIndices[i].comp = bi->ci[i];
            tHist.computeTags[0][i].comp = bi->ct0[i];
            tHist.computeTags[1][i].comp = bi->ct1[i];
            tHist.computeIndices[i].update(tHist.gHist);
            tHist.computeTags[0][i].update(tHist.gHist);
            tHist.computeTags[1][i].update(tHist.gHist);
        }
    }
}

int
VTAGEBase::bindex(Addr pc_in) const
{
    return ((pc_in) & ((ULL(1) << (logTagTableSizes[0])) - 1));
}

int
VTAGEBase::F(int A, int size, int bank) const
{
    int A1, A2;

    A = A & ((ULL(1) << size) - 1);
    A1 = (A & ((ULL(1) << logTagTableSizes[bank]) - 1));
    A2 = (A >> logTagTableSizes[bank]);
    A2 = ((A2 << bank) & ((ULL(1) << logTagTableSizes[bank]) - 1))
       + (A2 >> (logTagTableSizes[bank] - bank));
    A = A1 ^ A2;
    A = ((A << bank) & ((ULL(1) << logTagTableSizes[bank]) - 1))
      + (A >> (logTagTableSizes[bank] - bank));
    return (A);
}

// gindex computes a full hash of pc, ghist and pathHist
int
VTAGEBase::gindex(ThreadID tid, Addr pc, int bank) const
{
    int index;
    int hlen = (histLengths[bank] > pathHistBits) ? pathHistBits :
                                                    histLengths[bank];
    const unsigned int shiftedPc = pc;
    index =
        shiftedPc ^
        (shiftedPc >> ((int) abs(logTagTableSizes[bank] - bank) + 1)) ^
        threadHistory[tid].computeIndices[bank].comp ^
        F(threadHistory[tid].pathHist, hlen, bank);

    return (index & ((ULL(1) << (logTagTableSizes[bank])) - 1));
}


// Tag computation
uint16_t
VTAGEBase::gtag(ThreadID tid, Addr pc, int bank) const
{
    int tag = (pc) ^
              threadHistory[tid].computeTags[0][bank].comp ^
              (threadHistory[tid].computeTags[1][bank].comp << 1);

    return (tag & ((ULL(1) << tagTableTagWidths[bank]) - 1));
}


// Up-down saturating counter
template<typename T>
void
VTAGEBase::ctrUpdate(T & ctr, bool taken, int nbits)
{
    assert(nbits <= sizeof(T) << 3);
    if (taken) {
        if (ctr < ((1 << (nbits - 1)) - 1))
            ctr++;
    } else {
        if (ctr > -(1 << (nbits - 1)))
            ctr--;
    }
}

bool VTAGEBase::vtageTableUpdate(int bank,
    int index,
    InstSeqNum sn,
    uint64_t outcome,
    Info * bi,
    bool idiom
)
{
    bool blacklist = false;
    auto & entry = gtable[bank][index];
    entry.conf += ((rand() & (proba - 1)) == 0) &&
        (entry.conf < satConf) && (entry.pred == outcome);

    if (entry.pred != outcome)
    {
        blacklist = entry.conf == satConf;
        entry.conf = 0;
    }

    if (entry.conf == 0)
    {
        if (enableFullVP || (outcome <= 1) ||
            (enableHalfVP && is_9bit_signed(outcome)))
        {
            entry.pred = outcome;
        }
    }

    DPRINTF(
        ValuePred,
        "[sn:%lu] Tag matches, entry now has {%lu, %i}\n",
        sn,
        entry.pred,
        entry.conf
    );

    /*if (idiom)
    {
        entry.u = 0;
    }*/

    return blacklist;
}

// int8_t and int versions of this function may be needed
template void VTAGEBase::ctrUpdate(int8_t & ctr, bool taken, int nbits);
template void VTAGEBase::ctrUpdate(int & ctr, bool taken, int nbits);

// Up-down unsigned saturating counter
void
VTAGEBase::unsignedCtrUpdate(uint8_t & ctr, bool up, unsigned nbits)
{
    assert(nbits <= sizeof(uint8_t) << 3);
    if (up) {
        if (ctr < ((1 << nbits) - 1))
            ctr++;
    } else {
        if (ctr)
            ctr--;
    }
}

// Bimodal prediction
typename ValuePredictor<O3CPUImpl>::ValuePrediction
VTAGEBase::getBimodePred(Addr pc, Info* bi) const
{
    auto tag = (bi->pc >> (int) log2((double) btablePrediction.size())) & 0xf;

    auto & entry = btablePrediction[bi->bimodalIndex];
    const bool tag_matches = tag == entry.tag;

    DPRINTF(ValuePred, "BimodPred[%lu]: %lu, tag matches: %lu"
        " pred: %lu, conf: %u\n",
        bi->bimodalIndex,
        tag_matches,
        btablePrediction[bi->bimodalIndex].pred,
        btablePrediction[bi->bimodalIndex].conf
    );

    return {btablePrediction[bi->bimodalIndex].conf >= satConf && tag_matches,
        btablePrediction[bi->bimodalIndex].pred};
}


// Update the bimodal predictor: a hysteresis bit is shared among N prediction
// bits (N = 2 ^ logRatioBiModalHystEntries)
bool
VTAGEBase::baseUpdate(InstSeqNum sn, uint64_t outcome, bool idiom, Info* bi)
{
    bool blacklist = false;

    auto & entry = btablePrediction[bi->bimodalIndex];

    auto tag = (bi->pc >> (int) log2((double) btablePrediction.size())) & 0xf;
    const bool tag_matches = tag == entry.tag;

    const bool val_fits = (outcome <= 1) ||
        (enableHalfVP && is_9bit_signed(outcome)) ||
        enableFullVP;

    DPRINTF(ValuePred, "BaseUpdate[%lu]: Actual %lu, fit: %u\n",
        bi->bimodalIndex,
        outcome,
        val_fits);

    if (!tag_matches && (!val_fits || idiom))
    {
        // Do not overwrite an entry if we are not
        // getting 0x0 or 0x1 or 0x2 or 0x4 as a value
        DPRINTF(ValuePred, "Tag does not match, giving up\n");
        return false;
    }

    if (tag_matches)
    {
        entry.conf += ((rand() & (proba - 1)) == 0) &&
            (entry.conf < satConf) && (entry.pred == outcome);

        if (entry.pred != outcome)
        {
            DPRINTF(ValuePred, "BaseUpdate[%lu]: Entry conf: %u\n",
                bi->bimodalIndex,
                entry.conf);
            blacklist = entry.conf == satConf;
            entry.conf = 0;
        }

        if (entry.conf == 0)
        {
            if (val_fits)
            {
                entry.pred = outcome;
            }
        }

        DPRINTF(
          ValuePred,
          "BaseUpdate[%lu] : [sn:%lu] Tag matches, "
          "entry now has {%lu, %i}\n",
          bi->bimodalIndex,
          sn,
          entry.pred,
          entry.conf
        );
    }
    else
    {
        assert(val_fits);
        entry.conf = 0;
        entry.pred = outcome;
        entry.tag = tag;

        DPRINTF(
          ValuePred,
          "BaseUpdate[%lu] : [sn:%lu] Tag does not match, "
          "entry now has {%lu, %i}, tag %u\n",
          bi->bimodalIndex,
          sn,
          entry.pred,
          entry.conf,
          tag
        );
    }


    return blacklist;
}

// shifting the global history:  we manage the history in a big table in order
// to reduce simulation time
void
VTAGEBase::updateGHist(uint8_t * &h, bool dir, uint8_t * tab, int &pt)
{
    if (pt == 0) {
        DPRINTF(ValuePred, "Rolling over the histories\n");
         // Copy beginning of globalHistoryBuffer to end, such that
         // the last maxHist outcomes are still reachable
         // through pt[0 .. maxHist - 1].
         for (int i = 0; i < maxHist; i++)
             tab[histBufferSize - maxHist + i] = tab[i];
         pt =  histBufferSize - maxHist;
         h = &tab[pt];
    }
    pt--;
    h--;
    h[0] = (dir) ? 1 : 0;
}

void
VTAGEBase::calculateIndicesAndTags(ThreadID tid, Addr branch_pc,
                                  Info* bi)
{
    // computes the table addresses and the partial tags
    for (int i = 1; i <= nHistoryTables; i++) {
        tableIndices[i] = gindex(tid, branch_pc, i);
        bi->tableIndices[i] = tableIndices[i];
        tableTags[i] = gtag(tid, branch_pc, i);
        bi->tableTags[i] = tableTags[i];
    }
}

unsigned
VTAGEBase::getUseAltIdx(Info* bi)
{
    // There is only 1 counter on the base TAGE implementation
    return 0;
}

typename ValuePredictor<O3CPUImpl>::ValuePrediction
VTAGEBase::vtagePredict(ThreadID tid, Addr branch_pc, Info* bi)
{
    Addr pc = branch_pc;
    bi->pc = branch_pc;
    calculateIndicesAndTags(tid, pc, bi);

    bi->bimodalIndex = bindex(pc);

    bi->hitBank = 0;
    bi->altBank = 0;
    //Look for the bank with longest matching history
    for (int i = nHistoryTables; i > 0; i--) {
        if (noSkip[i] &&
            gtable[i][tableIndices[i]].tag == tableTags[i]) {
            bi->hitBank = i;
            bi->hitBankIndex = tableIndices[bi->hitBank];
            break;
        }
    }
    //Look for the alternate bank
    for (int i = bi->hitBank - 1; i > 0; i--) {
        if (noSkip[i] &&
            gtable[i][tableIndices[i]].tag == tableTags[i]) {
            bi->altBank = i;
            bi->altBankIndex = tableIndices[bi->altBank];
            break;
        }
    }
    //computes the prediction and the alternate prediction
    if (bi->hitBank > 0) {
        if (bi->altBank > 0) {
            bi->altPred =
              {gtable[bi->altBank][tableIndices[bi->altBank]].conf >= satConf,
              gtable[bi->altBank][tableIndices[bi->altBank]].pred};
            extraAltCalc(bi);
        }
        else {
            bi->altPred = getBimodePred(pc, bi);
        }

        bi->longestMatchPred =
            {gtable[bi->hitBank][tableIndices[bi->hitBank]].conf >= satConf,
            gtable[bi->hitBank][tableIndices[bi->hitBank]].pred};
        //bi->pseudoNewAlloc =
        //    abs(2 * gtable[bi->hitBank][bi->hitBankIndex].ctr + 1) <= 1;

        //if the entry is recognized as a newly allocated entry and
        //useAltPredForNewlyAllocated is positive use the alternate
        //prediction
        if ((useAltPredForNewlyAllocated[getUseAltIdx(bi)] < 0)
            || ! bi->pseudoNewAlloc) {
            bi->tagePred = bi->longestMatchPred;
            bi->provider = TAGE_LONGEST_MATCH;
        } else {
            bi->tagePred = bi->altPred;
            bi->provider = bi->altBank ? TAGE_ALT_MATCH
                                           : BIMODAL_ALT_MATCH;
        }
    } else {
        bi->altPred = getBimodePred(pc, bi);
        bi->tagePred = bi->altPred;
        bi->longestMatchPred = bi->altPred;
        bi->provider = BIMODAL_ONLY;
    }
        //end TAGE prediction
    return bi->tagePred;
}

void
VTAGEBase::adjustAlloc(bool & alloc, uint64_t outcome, uint64_t pred)
{
    // Nothing for this base class implementation
}

void
VTAGEBase::handleAllocAndUReset(bool alloc, uint64_t outcome, Info* bi,
                           int nrand)
{
    if (alloc) {
        // is there some "unuseful" entry to allocate
        uint8_t min = 1;
        for (int i = nHistoryTables; i > bi->hitBank; i--) {
            if (gtable[i][bi->tableIndices[i]].u < min) {
                min = gtable[i][bi->tableIndices[i]].u;
            }
        }

        // we allocate an entry with a longer history
        // to  avoid ping-pong, we do not choose systematically the next
        // entry, but among the 3 next entries
        int Y = nrand &
            ((ULL(1) << (nHistoryTables - bi->hitBank - 1)) - 1);
        int X = bi->hitBank + 1;
        if (Y & 1) {
            X++;
            if (Y & 2)
                X++;
        }
        // No entry available, forces one to be available
        if (min > 0) {
            gtable[X][bi->tableIndices[X]].u = 0;
        }


        //Allocate entries
        unsigned numAllocated = 0;
        for (int i = X; i <= nHistoryTables; i++) {
            if ((gtable[i][bi->tableIndices[i]].u == 0)) {
                gtable[i][bi->tableIndices[i]].tag = bi->tableTags[i];
                gtable[i][bi->tableIndices[i]].pred = outcome;
                gtable[i][bi->tableIndices[i]].conf = 0;
                ++numAllocated;
                if (numAllocated == maxNumAlloc) {
                    break;
                }
            }
        }
    }

    tCounter++;

    handleUReset();
}

void
VTAGEBase::handleUReset()
{
    //periodic reset of u: reset is not complete but bit by bit
    if ((tCounter & ((ULL(1) << logUResetPeriod) - 1)) == 0) {
        // reset least significant bit
        // most significant bit becomes least significant bit
        for (int i = 1; i <= nHistoryTables; i++) {
            for (int j = 0; j < (ULL(1) << logTagTableSizes[i]); j++) {
                resetUctr(gtable[i][j].u);
            }
        }
    }
}

void
VTAGEBase::resetUctr(uint8_t & u)
{
    u >>= 1;
}

bool
VTAGEBase::updatePredictor(ThreadID tid, InstSeqNum sn, uint64_t outcome,
    Info* bi, int nrand, uint64_t pred, bool idiom, bool preAdjustAlloc)
{
    // TAGE UPDATE
    // try to allocate a  new entries only if prediction was wrong
    bool alloc = (bi->tagePred.prediction != outcome) &&
        (bi->hitBank < nHistoryTables);

    alloc &= (enableFullVP || (outcome <= 1) ||
        (enableHalfVP && is_9bit_signed(outcome)));

    // Don't allc for dynamic idiom
    alloc &= !idiom;

    if (preAdjustAlloc) {
        adjustAlloc(alloc, outcome, pred);
    }

    if (bi->hitBank > 0) {
        // Manage the selection between longest matching and alternate
        // matching for "pseudo"-newly allocated longest matching entry
        bool PseudoNewAlloc = bi->pseudoNewAlloc;
        // an entry is considered as newly allocated if its prediction
        // counter is weak
        if (PseudoNewAlloc) {
            if (bi->longestMatchPred.prediction == outcome) {
                alloc = false;
            }
            // if it was delivering the correct prediction, no need to
            // allocate new entry even if the overall prediction was false
            if (bi->longestMatchPred.prediction != bi->altPred.prediction) {
                ctrUpdate(
                    useAltPredForNewlyAllocated[getUseAltIdx(bi)],
                    bi->altPred.prediction == outcome, useAltOnNaBits);
            }
        }
    }

    if (!preAdjustAlloc) {
        adjustAlloc(alloc, outcome, pred);
    }

    handleAllocAndUReset(alloc, outcome, bi, nrand);

    return handleVTAGEUpdate(sn, outcome, bi, idiom);
}

bool
VTAGEBase::handleVTAGEUpdate(InstSeqNum sn, uint64_t outcome,
Info* bi, bool idiom)
{
    bool blacklist = false;

    if (bi->hitBank > 0) {
        DPRINTF(ValuePred, "Updating tag table entry (%d,%d) for [sn:%lu]\n",
                bi->hitBank, bi->hitBankIndex, sn);
        blacklist |= vtageTableUpdate(
            bi->hitBank,
            bi->hitBankIndex,
            sn,
            outcome,
            bi,
            idiom
        );

        // if the provider entry is not certified to be useful also update
        // the alternate prediction
        if (gtable[bi->hitBank][bi->hitBankIndex].u == 0) {
            if (bi->altBank > 0) {
                blacklist |= vtageTableUpdate(
                    bi->altBank,
                    bi->altBankIndex,
                    sn,
                    outcome,
                    bi,
                    idiom
                );
                DPRINTF(ValuePred, "Updating tag table entry (%d,%d) for"
                        " [sn:%lu]\n", bi->hitBank, bi->hitBankIndex,
                        sn);
            }
            if (bi->altBank == 0) {
                blacklist |= baseUpdate(sn, outcome, idiom,bi);
            }
        }

        // update the u counter
        if (bi->tagePred.prediction != bi->altPred.prediction) {
            unsignedCtrUpdate(
                gtable[bi->hitBank][bi->hitBankIndex].u,
                bi->tagePred.prediction == outcome && !idiom,
                tagTableUBits);
        }
    } else {
        blacklist |= baseUpdate(sn, outcome, idiom, bi);
    }

    return blacklist;
}

void
VTAGEBase::updateHistories(ThreadID tid, Addr branch_pc, bool taken,
                          Info* bi)
{
    ThreadHistory& tHist = threadHistory[tid];
    //  UPDATE HISTORIES
    bool pathbit = ((branch_pc >> instShiftAmt) & 1);

    // First, save things
    bi->ptGhist = tHist.ptGhist;
    bi->pathHist = tHist.pathHist;

    DPRINTF(ValuePred, "saving phistptr: 0x%lx phist : 0x%lx\n",
        bi->ptGhist,
        bi->pathHist);

    // Then, update things
    updateGHist(tHist.gHist, taken, tHist.globalHistory, tHist.ptGhist);
    tHist.pathHist = (tHist.pathHist << 1) + pathbit;
    tHist.pathHist = (tHist.pathHist & ((ULL(1) << pathHistBits) - 1));

    //prepare next index and tag computations for user branchs
    for (int i = 1; i <= nHistoryTables; i++)
    {
        bi->ci[i]  = tHist.computeIndices[i].comp;
        bi->ct0[i] = tHist.computeTags[0][i].comp;
        bi->ct1[i] = tHist.computeTags[1][i].comp;

        DPRINTF(ValuePred, "Saving  for index %i\n", i);
        DPRINTF(ValuePred, " tHist.computeIndices[%i].comp : 0x%lx\n",
            i,
            tHist.computeIndices[i].comp);
        DPRINTF(ValuePred, " tHist.computeTags[0][%i].comp : 0x%lx\n",
            i,
             tHist.computeTags[0][i].comp);
        DPRINTF(ValuePred, "tHist.computeTags[1][%i].comp : 0x%lx\n",
            i,
            tHist.computeTags[1][i].comp);

        tHist.computeIndices[i].update(tHist.gHist);
        tHist.computeTags[0][i].update(tHist.gHist);
        tHist.computeTags[1][i].update(tHist.gHist);

        DPRINTF(ValuePred, "Updating  for index %i\n", i);
        DPRINTF(ValuePred, " tHist.computeIndices[%i].comp : 0x%lx\n",
            i,
            tHist.computeIndices[i].comp);
        DPRINTF(ValuePred, " tHist.computeTags[0][%i].comp : 0x%lx\n",
            i,
             tHist.computeTags[0][i].comp);
        DPRINTF(ValuePred, "tHist.computeTags[1][%i].comp : 0x%lx\n",
            i,
            tHist.computeTags[1][i].comp);
    }
    DPRINTF(ValuePred, "Updating global histories with branch:%lx; taken?:%d, "
            "path Hist: %x; pointer:%d\n", branch_pc, taken, tHist.pathHist,
            tHist.ptGhist);
    assert(threadHistory[tid].gHist ==
            &threadHistory[tid].globalHistory[threadHistory[tid].ptGhist]);
}

void
VTAGEBase::restoreHistories(ThreadID tid, VTAGEBase::Info *bi)
{
    if (!speculativeHistUpdate) {
        /* If there are no speculative updates, no actions are needed */
        return;
    }

    ThreadHistory& tHist = threadHistory[tid];
    DPRINTF(
        ValuePred,
        "Restoring branch info: PathHistory:0x%lx, "
        "pointer:0x%lx\n",
        bi->pathHist,
        bi->ptGhist
    );
    tHist.pathHist = bi->pathHist;
    tHist.ptGhist = bi->ptGhist;
    tHist.gHist = &(tHist.globalHistory[tHist.ptGhist]);
    for (int i = 1; i <= nHistoryTables; i++) {
        tHist.computeIndices[i].comp = bi->ci[i];
        tHist.computeTags[0][i].comp = bi->ct0[i];
        tHist.computeTags[1][i].comp = bi->ct1[i];
        /*tHist.computeIndices[i].update(tHist.gHist);
        tHist.computeTags[0][i].update(tHist.gHist);
        tHist.computeTags[1][i].update(tHist.gHist);*/

        DPRINTF(ValuePred, "Restoring for index %i\n", i);
        DPRINTF(ValuePred, "computeIndices[%i] : 0x%lx\n",
            i,
            tHist.computeIndices[i].comp);
        DPRINTF(ValuePred, " tHist.computeTags[0][%i].comp : 0x%lx\n",
            i,
             tHist.computeTags[0][i].comp);
        DPRINTF(ValuePred, "tHist.computeTags[1][%i].comp : 0x%lx\n",
            i,
            tHist.computeTags[1][i].comp);
    }
}

void
VTAGEBase::extraAltCalc(Info* bi)
{
    // do nothing. This is only used in some derived classes
    return;
}

void
VTAGEBase::updateStats(uint64_t outcome, Info* bi)
{
    if (outcome == bi->tagePred.prediction) {
        // correct prediction
        switch (bi->provider) {
          case BIMODAL_ONLY: stats.bimodalProviderCorrect++; break;
          case TAGE_LONGEST_MATCH: stats.longestMatchProviderCorrect++; break;
          case BIMODAL_ALT_MATCH:
            stats.bimodalAltMatchProviderCorrect++;
            break;
          case TAGE_ALT_MATCH: stats.altMatchProviderCorrect++; break;
        }
    } else {
        // wrong prediction
        switch (bi->provider) {
          case BIMODAL_ONLY: stats.bimodalProviderWrong++; break;
          case TAGE_LONGEST_MATCH:
            stats.longestMatchProviderWrong++;
            if (bi->altPred.prediction == outcome) {
                stats.altMatchProviderWouldHaveHit++;
            }
            break;
          case BIMODAL_ALT_MATCH:
            stats.bimodalAltMatchProviderWrong++;
            break;
          case TAGE_ALT_MATCH:
            stats.altMatchProviderWrong++;
            break;
        }

        switch (bi->provider) {
          case BIMODAL_ALT_MATCH:
          case TAGE_ALT_MATCH:
            if (bi->longestMatchPred.prediction == outcome) {
                stats.longestMatchProviderWouldHaveHit++;
            }
        }
    }

    switch (bi->provider) {
      case TAGE_LONGEST_MATCH:
      case TAGE_ALT_MATCH:
        stats.longestMatchProvider[bi->hitBank]++;
        stats.altMatchProvider[bi->altBank]++;
        break;
    }
}

unsigned
VTAGEBase::getGHR(ThreadID tid, Info *bi) const
{
    unsigned val = 0;
    for (unsigned i = 0; i < 32; i++) {
        // Make sure we don't go out of bounds
        int gh_offset = bi->ptGhist + i;
        assert(&(threadHistory[tid].globalHistory[gh_offset]) <
               threadHistory[tid].globalHistory + histBufferSize);
        val |= ((threadHistory[tid].globalHistory[gh_offset] & 0x1) << i);
    }

    return val;
}

VTAGEBase::VTAGEBaseStats::VTAGEBaseStats(
    Stats::Group *parent, unsigned nHistoryTables)
    : Stats::Group(parent),
      ADD_STAT(longestMatchProviderCorrect, "Number of times TAGE Longest"
          " Match is the provider and the prediction is correct"),
      ADD_STAT(altMatchProviderCorrect, "Number of times TAGE Alt Match"
          " is the provider and the prediction is correct"),
      ADD_STAT(bimodalAltMatchProviderCorrect, "Number of times TAGE Alt"
          " Match is the bimodal and it is the provider and the prediction"
          " is correct"),
      ADD_STAT(bimodalProviderCorrect, "Number of times there are no"
          " hits on the TAGE tables and the bimodal prediction is correct"),
      ADD_STAT(longestMatchProviderWrong, "Number of times TAGE Longest"
          " Match is the provider and the prediction is wrong"),
      ADD_STAT(altMatchProviderWrong, "Number of times TAGE Alt Match is"
          " the provider and the prediction is wrong"),
      ADD_STAT(bimodalAltMatchProviderWrong, "Number of times TAGE Alt Match"
          " is the bimodal and it is the provider and the prediction is"
          " wrong"),
      ADD_STAT(bimodalProviderWrong, "Number of times there are no hits"
          " on the TAGE tables and the bimodal prediction is wrong"),
      ADD_STAT(altMatchProviderWouldHaveHit, "Number of times TAGE"
          " Longest Match is the provider, the prediction is wrong and"
          " Alt Match prediction was correct"),
      ADD_STAT(longestMatchProviderWouldHaveHit, "Number of times"
          " TAGE Alt Match is the provider, the prediction is wrong and"
          " Longest Match prediction was correct"),
      ADD_STAT(longestMatchProvider, "TAGE provider for longest match"),
      ADD_STAT(altMatchProvider, "TAGE provider for alt match")
{
    longestMatchProvider.init(nHistoryTables + 1);
    altMatchProvider.init(nHistoryTables + 1);
}

int8_t
VTAGEBase::getCtr(int hitBank, int hitBankIndex) const
{
    fatal("VTAGEBase::getCtr() unimplemented");
    return 0x0;
}

unsigned
VTAGEBase::getTageCtrBits() const
{
    return tagTableCounterBits;
}

int
VTAGEBase::getPathHist(ThreadID tid) const
{
    return threadHistory[tid].pathHist;
}

bool
VTAGEBase::isSpeculativeUpdateEnabled() const
{
    return speculativeHistUpdate;
}

size_t
VTAGEBase::getSizeInBits() const {
    size_t bits = 0;
    for (int i = 1; i <= nHistoryTables; i++) {
        bits += (1 << logTagTableSizes[i]) *
            (tagTableCounterBits + tagTableUBits + tagTableTagWidths[i]);
    }
    uint64_t bimodalTableSize = ULL(1) << logTagTableSizes[0];
    bits += numUseAltOnNa * useAltOnNaBits;
    bits += bimodalTableSize;
    bits += (bimodalTableSize >> logRatioBiModalHystEntries);
    bits += histLengths[nHistoryTables];
    bits += pathHistBits;
    bits += logUResetPeriod;
    return bits;
}

/*VTAGEBase *
VTAGEBaseParams::create() const
{
    return new VTAGEBase(*this);
}*/
