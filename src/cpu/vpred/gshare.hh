/*
 * Copyright (c) 2020 CNRS
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#pragma once

#include "cpu/o3/impl.hh"
#include "cpu/vpred/lvp.hh"
#include "debug/ValuePred.hh"
#include "params/GShareValuePredictor.hh"

class GShareValuePredictor : public LastValuePredictor
{
  protected:
    struct Entry
    {
        uint64_t m_prediction = 0;
        uint64_t m_tag = 0x0;
        int m_conf = 0;

        Entry() = default;

        Entry(uint64_t val, uint64_t tag)
            : m_prediction(val)
            , m_tag(tag)
            , m_conf(0)
            {}
    };

    struct HistCheckpoint
    {
        InstSeqNum m_seqnum;
        uint64_t m_ghist;
        Addr m_table_index = 0xdeadbeef;

        HistCheckpoint() = default;

        HistCheckpoint(InstSeqNum sn, uint64_t hist)
            : m_seqnum(sn)
            , m_ghist(hist)
            {}

        HistCheckpoint(InstSeqNum sn, uint64_t hist, Addr index)
            : m_seqnum(sn)
            , m_ghist(hist)
            , m_table_index(index)
            {}

        bool operator<(const InstSeqNum & sn)
        {
            return m_seqnum < sn;
        }
    };

    int m_ghist_length;
    uint64_t m_ghist_mask = 0x0;
    uint64_t m_ghist = 0x0;
    std::deque<HistCheckpoint> m_history;

  public:
    GShareValuePredictor(const GShareValuePredictorParams &params)
        : LastValuePredictor(params)
        , m_ghist_length(params.ghist_length)
        , m_ghist_mask((0x1 << m_ghist_length) - 1)
    {
    }

    virtual ~GShareValuePredictor() = default;

    virtual uint64_t array_index(Addr inst_addr, int uop, int index)
    const override
    {
        return ((inst_addr >> 2) ^
            (index << 4) ^ (uop << 2) ^
            (m_ghist & m_ghist_mask)) &
            m_index_mask;
    }

    virtual uint64_t array_tag(Addr inst_addr)
    const override
    {
        return ((inst_addr >> 2) >> m_log2_sets) & m_tag_mask;
    }

    virtual void updateBranch(
      InstSeqNum sn,
      Addr pc,
      bool direction,
      Addr target,
      ThreadID tid,
      bool uncond = false
    )
    override final
    {
        if (uncond)
        {
            return;
        }

        m_history.emplace_back(sn, m_ghist);

        DPRINTF(
            ValuePred,
            "Saving GHIST: 0x%lx for inst [sn:%lu]\n",
            m_ghist,
            sn
        );

        m_ghist = (m_ghist << 1) | direction;
        DPRINTF(ValuePred, "GHIST is now : 0x%lx\n", m_ghist);
    }

    virtual ValuePrediction derivedLookup(InstSeqNum sn, Addr inst_addr,
    ThreadID tid, int uop = 0, int index = 0) override final;

    virtual void derivedSquash(InstSeqNum seq_num, ThreadID tid)
    override final
    {
        while (!m_history.empty() && m_history.back().m_seqnum > seq_num)
        {
            DPRINTF(
                ValuePred,
                "Squashing [sn:%lu] in gshare\n",
                m_history.back().m_seqnum
            );

            m_ghist = m_history.back().m_ghist;
            m_history.pop_back();
        }
        DPRINTF(ValuePred, "GHIST restored to 0x%lx\n", m_ghist);
    }

    virtual bool derivedUpdate(InstSeqNum sn, Addr inst_addr,
    uint64_t value, bool idiom,
    ThreadID tid, int uop = 0, int index = 0) override final;
};
