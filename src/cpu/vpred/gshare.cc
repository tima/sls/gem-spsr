/*
 * Copyright (c) 2014 ARM Limited
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu/vpred/gshare.hh"

#include "debug/ValuePred.hh"

typename ValuePredictor<O3CPUImpl>::ValuePrediction
GShareValuePredictor::derivedLookup(
    InstSeqNum sn,
    Addr instr_addr,
    ThreadID tid,
    int uop,
    int index
)
{
    vpStats.numPredictions += (index == 0);

    m_history.emplace_back(sn, m_ghist, array_index(instr_addr, uop, index));

    const auto & entry =
        m_table[array_index(instr_addr, uop, index)];

    const bool tag_matches = entry.m_tag == array_tag(instr_addr);

    return {tag_matches && entry.m_conf >= 31, entry.m_prediction};
}

bool GShareValuePredictor::derivedUpdate(InstSeqNum sn, Addr inst_addr,
uint64_t value, bool idiom, ThreadID tid, int uop, int index)
{
    bool blacklist = false;

    while (!m_history.empty() && m_history.front().m_seqnum < sn)
    {
        m_history.pop_front();
    }

    assert(!m_history.empty());

    auto & hist_entry = m_history.front();

    assert(hist_entry.m_seqnum == sn);

    vpStats.numUpdates += (index == 0);

    auto & entry =
        m_table[hist_entry.m_table_index];

    const bool tag_matches = entry.m_tag == array_tag(inst_addr);

    DPRINTF(ValuePred, "Updating : Entry has 0x%lx, got 0x%lx\n",
        entry.m_prediction,
        value);

    if (!tag_matches && (value > 1) && !enableFullVP)
    {
        // Do not overwrite an entry if we are not
        // getting 0x0 or 0x1 as a value
        m_history.pop_front();
        return false;
    }

    if (tag_matches)
    {
        entry.m_conf += (entry.m_conf < 31) && (entry.m_prediction == value);

        if (entry.m_prediction != value)
        {
            blacklist = entry.m_conf == 31;
            entry.m_conf = 0;
        }

        vpStats.numCorrectPredictions += entry.m_prediction == value;
        vpStats.numIncorrectPredictions += entry.m_prediction != value;

        if (entry.m_conf == 0)
        {
            entry.m_prediction = value;
        }
    }
    else
    {
       entry.m_conf = std::max(entry.m_conf - 8, 0);
       if (entry.m_conf == 0)
       {
           ++vpStats.numReplacements;
           entry = {value, array_tag(inst_addr)};
       }
    }

    m_history.pop_front();

    return blacklist;
}

GShareValuePredictor *
GShareValuePredictorParams::create() const
{
    return new GShareValuePredictor(*this);
}
