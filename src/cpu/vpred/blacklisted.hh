/*
 * Copyright (c) 2020 CNRS
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#pragma once

#include "cpu/o3/impl.hh"
#include "cpu/vpred/vp.hh"
#include "params/BlacklistedPredictor.hh"

class BlacklistedPredictor : public SimObject, public ValuePredictor<O3CPUImpl>
{
    struct BlackListEntry
    {
      uint64_t pc;
      uint64_t added_at;

      BlackListEntry(Addr addr, uint64_t tick)
      : pc(addr)
      , added_at(tick)
      {}

      BlackListEntry() = default;
    };

    struct PotentialBlackListEntry
    {
      uint64_t pc;
      uint64_t mispred_count;

      PotentialBlackListEntry(Addr addr)
        : pc(addr)
        , mispred_count(1)
        {}
    };

    int m_blacklist_size = 32;
    std::map<uint64_t, BlackListEntry> m_blacklist_map;
    std::deque<BlackListEntry> m_blacklist_vec;

    int m_blacklist_threshold = 64;
    int m_pot_blacklist_size = 32;
    std::vector<PotentialBlackListEntry> m_potential_blacklist;

    uint64_t generateBlacklistID(Addr pc, int uop, int index)
    {
      return pc ^ (((uint64_t) uop) << 56) ^ (((uint64_t) index) << 60);
    }

  protected:
    bool enableBlacklist;
    unsigned satConf = 0;
    unsigned proba = 0;

    struct VPStatGroup : public Stats::Group
    {
      VPStatGroup(Stats::Group *parent);

      Stats::Scalar numPredictions;
      Stats::Scalar numBlacklisted;
      Stats::Scalar numPredIgnoredBlacklist;
      Stats::Scalar numUpdates;
      Stats::Scalar numReplacements;
      Stats::Scalar numCorrectPredictions;
      Stats::Scalar numIncorrectPredictions;
      Stats::Scalar numUsedCorrectPredictions;
      Stats::Scalar numUsedIncorrectPredictions;
    } vpStats;


  public:
    BlacklistedPredictor(const BlacklistedPredictorParams &params)
        : SimObject(params)
        , ValuePredictor(params.enableFullVP,
            params.enableHalfVP)
        , enableBlacklist(params.enableBlacklist)
        , satConf(params.satConf)
        , proba(params.incProba)
        , vpStats(this)
    {
    }

    virtual ~BlacklistedPredictor() = default;

    virtual const std::string name() const
    override
    {
      return ValuePredictor<O3CPUImpl>::name();
    }


    virtual ValuePrediction filteredLookup(InstSeqNum sn, Addr inst_addr,
    ThreadID tid, int uop = 0, int index = 0) override final;

    virtual void filteredUpdate(InstSeqNum sn, bool mispred, bool correct,
    bool idiom,
    Addr inst_addr, uint64_t value, ThreadID tid,
    int uop = 0, int index = 0) override final;

    void updateBlacklist(Addr pc);

    bool moveToBlacklist(
        const typename std::vector<PotentialBlackListEntry>::iterator & it);

    // Just to deal with Python for now
    virtual ValuePrediction derivedLookup(InstSeqNum sn, Addr inst_addr,
    ThreadID tid, int uop = 0, int index = 0) override
    {
        return {0,0};
    }

    virtual void derivedSquash(InstSeqNum seq_num, ThreadID tid)
    override
    {
        // This is LVP, we are not doing anything
    }

    virtual bool derivedUpdate(InstSeqNum sn, Addr inst_addr,
    uint64_t value, bool idiom,
    ThreadID tid, int uop = 0, int index = 0) override
    {
        return false;
    }
};
